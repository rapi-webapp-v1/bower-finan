Change Log - webap RAPI FINAN
=============================

# [Unreleased](https://bitbucket.org/rapi3/commits/all)

[Full Changelog](hhttps://bitbucket.org/rapi3/branches/compare/v1.0.0%0Dmaster)

#Broken

## Bug Fixes:

## Features:

# [0.0.3](https://bitbucket.org/rapi3/commits/tag/v0.0.3) (2017-03-14)
[Full Changelog](https://bitbucket.org/rapi3/branches/compare/v0.0.3%0Dmaster#diff)

## Features:
- **RFIN-154** Calculo de juros automatico
- **RFIN-182** fazer modal de cancelamento
- **RFIN-142** detalhe das parcelas dentro do modal
- **Dialog Baixa** Novo modal separado para baixa de programação
- **report** Relatorios de DRE / Extrato lançamento / Contas a pagar e receber / Saldo de contas
- **transferencia** suporte a transferencia entre contas


# [0.0.2](https://bitbucket.org/rapi3/commits/tag/v0.0.2) (2017-03-01)
[Full Changelog](https://bitbucket.org/rapi3/branches/compare/v0.0.2%0Dmaster#diff)

#Broken
- **Programar** Removido campo marcador do form

## Bug Fixes:
- **Entidade** Favorecido não deve aparecer data nasc, sexo e etc quando for tipo juridico
- **Programar** Listar ordenado os subgrupos de centro de custo

## Features:
- **reconciliação** Mudar termo de reconciliação => para "Conciliação" com subtitle "Bloqueio de caixa "
- **Programar** Modal conta pag. add btn Salvar e cadastrar novo
- **mask** tratado os campos CNPJ, CPF, telefones e CEP


# [0.0.1](https://bitbucket.org/rapi3/commits/tag/v0.0.1) (2017-02-17)
[Full Changelog](https://bitbucket.org/rapi3/branches/compare/v0.0.1%0Dmaster#diff)
> First
