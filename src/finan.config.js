(function ()
{
    'use strict';

    angular
            .module('rapi.finan.config', [ 'rapi.fuse.xsession'])
            .config(config);

    /** @ngInject */
    function config(xSessionProvider)
    {


        xSessionProvider.setQueryModel(['Livro', function (Livro) {
                return Livro.listSessions();
            }]);

        xSessionProvider.configure({
            home: 'xsession.dashboards',
            keyHeader: 'X-SESSION',
            nameToolbar: 'Livro',
            keyStorange: 'session',
            labels: {
                title: 'Olá,',
                content: 'Nós percebemos que você têm acesso a mais de um livro, com qual delas deseja trabalhar agora?',
                title_empty_state: 'Você não tem acesso a nenhuma livro.',
                content_empty_state: 'Entre em contato com o seu gerente para solicitar a permissão de acesso.'
            },
            //Mescla os objeto com o request.header Ex: {'FOO':'bar'}
            prependHeaders: {
//                'APP_ID': 'finan',
//                'APP_SECRET': 'app.dashboards'
            }
        });
    }

})();
