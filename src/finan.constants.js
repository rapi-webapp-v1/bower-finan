(function () {
    'use strict';

    angular
            .module('rapi.finan.constants', [])
            .constant('Variables', getVariables())
            .constant('FinanPaths', getFinanPaths());

    function getVariables()
    {
        return {
            'NAME': 'Financeiro',
            'NAME_MODULE': 'rapi.finan',
            'STATE_HOME': 'app.dashboards',
            'frequencia': [
                {
                    'key': 'unica',
                    'label': 'Apenas uma vez'
                },
                {
                    'key': RRule.DAILY,
                    'label': 'Diariamente'
                },
                {
                    'key': RRule.WEEKLY,
                    'label': 'Semanalmente'
                },
                {
                    'key': RRule.MONTHLY,
                    'label': 'Mensalmente'
                },
                {
                    'key': RRule.YEARLY,
                    'label': 'Anualmente'
                }
            ],
            'situacao': [
                {
                    'key': 'pendente',
                    'label': 'Pendente'
                },
                {
                    'key': 'compensado',
                    'label': 'Compensado'
                },
                {
                    'key': 'atrasado',
                    'label': 'Atrasado'
                },
                {
                    'key': 'cancelado',
                    'label': 'Cancelado'
                }
            ],
            'tipo_estado_civil': {
                'solteiro': 'Solteiro (ª)',
                'casado': 'Casado (ª)',
                'separado': 'Separado (ª)',
                'divorciado': 'Divorciado (ª)',
                'viuvo': 'Viúvo (ª)'
            },
            'dates' : [
                {
                    "label": "Criaçao",
                    "param": 'created_at'
                },
                {
                    "label": "Ediçao",
                    "param": 'updated_at'
                },
                {
                    "label": "Emissão",
                    "param": 'dt_emissao'
                },
                {
                    "label": "Competência",
                    "param": 'dt_competencia'
                },
                {
                    "label": "Compensado",
                    "param": 'dt_compensado'
                },
                {
                    "label": "Vencimento",
                    "param": 'dt_prevista'
                },
                {
                    "label": "Reconciliação",
                    "param": 'dt_reconciliado'
                }
            ]
        };
    }

    function getFinanPaths()
    {
        return {
           //'BASE_PATH' : 'app/package/main/'
//           'BASE_PATH' : 'rapi/rapi-finan/main/'
           'BASE_PATH' : '/app/external/finan/main/'
        };
    }



})();
