(function() {
  'use strict';

  angular
    .module('rapi.finan')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider, msNavigationServiceProvider, FinanPaths) {

    var path = 'rapi/rapi-fuse/layouts';
    var pathPackage = FinanPaths.BASE_PATH;


//        $stateProvider
//                .state('app', {
//                    abstract: true,
//                    views: {
//                        'main@': {
//                            templateUrl: 'app/external/fuse/layouts/vert-nav-comp-toolbar/vert-nav-comp-toolbar.html',
//                            controller: 'LayoutsController as vm'
//                        },
//                       'toolbar@app': {
//                            templateUrl: pathPackage + 'toolbar/layouts/toolbar2/toolbar.html',
//                            controller: 'ToolbarController as vm'
//                        },
//                         'navigation@app': {
//                            templateUrl:  'app/external/fuse/layouts/partials/navigation/vertical-navigation2.html',
//                            controller: 'NavigationController as vm'
//                        },
//                        'quickPanel@app': {
//                            templateUrl: 'app/external/fuse/layouts/partials/quickpanel/quickpanel.html',
//                            controller: 'QuickPanelController as vm'
//                        }
//                    }
//                });


    $stateProvider
      .state('popup', {
        url: '/popup',
        abstract: true,
        views: {
          'main@': {
            template: '<div ui-view="content"></div>',
            controller: 'MainController as vm'
          }
        }
      });


        msNavigationServiceProvider.saveItem('configuracoes', {
            title: 'Configurações',
            icon: 'icon-cog',
 
            weight: 90
        });
        msNavigationServiceProvider.saveItem('configuracoes.cidade', {
            title: 'CITIES.MENU',
            icon: 'icon-city',
            state: 'app.cities.tabbed',
            weight: 88
        });
        msNavigationServiceProvider.saveItem('configuracoes.user', {
            title: 'Usuários',
            icon: 'icon-account',
            state: 'app.user',
            weight: 10
        });
  }

})();
