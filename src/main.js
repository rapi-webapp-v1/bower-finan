(function () {
    'use strict';
    /**
     * Main module of the Fuse
     */
    angular
            .module('rapi.finan', [
                //RAPI FUSE
                'rapi.fuse',
                'rapi.fuse.cities',
                'rapi.fuse.report',
                'rapi.fuse.metas',
                'rapi.finan.config',
                'rapi.fuse.xsession',
                'rapi.fuse.user',
                //CONFIG
                'rapi.finan.constants',
                //Libs
                'ngMask',
                //DOMAINS
                'main.livro',
                'main.encargo',
                'main.centro_custo',
                'main.banco',
                'main.conta',
                'main.plano_conta',
                'main.tipo_conta',
                'main.forma_pagamento',
                'main.programar',
                'main.lancamento',
                'main.entidade',
                'main.reconciliacao',
                'main.relatorios',
//                'main.home',
                'main.toolbar',
                'main.transferencia',


                //APPs
                'main.dashboards'
            ]);

})();
