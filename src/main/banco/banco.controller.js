(function () {
    'use strict';

    angular
            .module('main.banco')
            .controller('BancoController', BancoController);

    /** @ngInject */
    function BancoController(Banco, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'nome', //EX: nome,-created_at
            'q': null
        };

        // Methods
        vm.showCreate = showCreate;
        vm.showEdit = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search = search;
        vm.setPage = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Banco.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Banco.pagination;
            });


        }


        function setPage(p)
        {
            Banco.setPage(p);
            activate();
        }

        function search()
        {
            Banco.setPage(1);
            activate();
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            Banco.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            Banco.dialog.edit(row, vm.archive, ev);
        }

        function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('BANCO.TITLE_CONFIRM_DELETE'), $translate.instant('BANCO.MSG_CONFIRM_DELETE') + item.nome + '?', $event)
                    .then(function () {
                        Banco.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }


})();
