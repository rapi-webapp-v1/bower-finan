(function ()
{
    'use strict';

    angular
            .module('main.banco', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
                .state('app.banco', {
                    url: '/bancos',
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'banco/banco-archive.html',
                            controller: 'BancoController as vm'
                        }
                    }
                });


        msNavigationServiceProvider.saveItem('configuracoes.banco', {
            title: 'BANCO.MENU',
            icon: 'icon-bank',
            state: 'app.banco',
            weight: 50
        });

    }
})();
