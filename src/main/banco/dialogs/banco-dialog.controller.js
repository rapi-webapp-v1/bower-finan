(function() {
  'use strict';

  angular
    .module('main.banco')
    .controller('BancoDialogController', BancoDialogController);

  /** @ngInject */
  function BancoDialogController($mdDialog, Banco, row, archive) {

    var vm = this;

    //Data
    vm.isNew = true;
    vm.row   = angular.copy(row);
    vm.title = null;

    //Methods
    vm.closeDialog = closeDialog;
    vm.update = update;
    vm.save = save;

    activate();
    //-----------------------------------

    function activate() {
      archive = archive || [];

        if (vm.row && vm.row.id) {
            vm.isNew = false;
        }
    }

    function update() {
      Banco.update(row.id, vm.row).then(function(result) {
        if (result) {
          angular.extend(row, result);
          closeDialog(row);
        }
      });
    }

    function save() {
      Banco.save(vm.row).then(function(result) {
        if (result) {
          archive.push(result);
          closeDialog(result);
        }
      });
    }

    function closeDialog(params) {
      $mdDialog.hide(params);
    }

  }


})();
