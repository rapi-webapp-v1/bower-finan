(function () {
    'use strict';

    angular
            .module('main.centro_custo')
            .controller('CentroCustoController', CentroCustoController);

    /** @ngInject */
    function CentroCustoController(CentroCusto, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': null,
            'sort': 'nome', //EX: nome,-created_at
            'q': null,
            'filter': 'onlyPai',
            'include': 'filhos'
        };

        // Methods
        vm.showCreate = showCreate;
        vm.showEdit = showEdit;
        vm.confirmDelete = confirmDelete;
//        vm.search = search;
        vm.setPage = setPage;

        activate();

        //-----------------------
        function activate() 
        {
            vm.loading = true;
            //vm.archive = null;

            CentroCusto.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = CentroCusto.pagination;
            });

        }

        function setPage(p)
        {
            CentroCusto.setPage(p);
            activate();
        }

//        function search()
//        {
//            CentroCusto.setPage(1);
//            activate();
//        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev, pai) 
        {
            CentroCusto.dialog.create(vm.archive, ev, null, pai);
        }

        function showEdit(row, ev)
        {
            CentroCusto.dialog.edit(row, vm.archive, ev);
        }

        function confirmDelete(item, $event)
        {
            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('CENTRO_CUSTO.TITLE_CONFIRM_DELETE'), $translate.instant('CENTRO_CUSTO.MSG_CONFIRM_DELETE') + item.nome + '?', $event)
                    .then(function () {
                        CentroCusto.remove(item.id).then(activate);
                    });

        }

    }


})();
