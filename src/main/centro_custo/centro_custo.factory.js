(function () {
    'use strict';

    angular
            .module('main.centro_custo')
            .factory('CentroCusto', centro_custoService);


    /** @ngInject */
    function centro_custoService($mdDialog, w3Resource, FinanPaths) {

        var urlApi = '/rapi/finan/centro-custos';
        var path_pack = FinanPaths.BASE_PATH;
        var service = {
            onlyPai: onlyPai,
            querySearch: querySearch,
            dialog: {
                create: showCreateDialog,
                edit: showDialog
            }
        };

        return w3Resource.extend(urlApi, service);

        //----------------------
        function onlyPai()
        {
            var params = {
                'filter': 'onlyPai',
                'sort': 'nome',
                'include': 'filhos'
            };

            return this.search(params);
        }

        function querySearch(query)
        {
            var params = {
                q: query,
                take: 10
            };
            
            return this.search(params);
        }

        // DIALOGS

        /**
         * archive: array com a lista de itens
         * data: pode ser undfined, porem se passsar um objeto ja vai abrir o dialog com dados preenchidos, ideal para passar includes tmb
         * */
        function showCreateDialog(archive, ev, data, pai)
        {
            return showDialog(data, archive, ev, pai);
        }

        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(row, archive, ev, pai)
        {
            return $mdDialog.show({
                controller: 'CentroCustoDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'centro_custo/dialogs/centro_custo-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row,
                    'archive': archive,
                    'pai': pai
                }
            });

        }

    }

})();
