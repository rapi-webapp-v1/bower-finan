(function ()
{
    'use strict';

    angular
            .module('main.centro_custo', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider,FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
                .state('xsession.centro_custo', {
                    url: '/centro_custos',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'centro_custo/centro_custo-archive.html',
                            controller: 'CentroCustoController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('configuracoes.centro_custo', {
            title: 'CENTRO_CUSTO.MENU',
            icon: 'icon-book-multiple',
            state: 'xsession.centro_custo',
            weight: 20
        });

    }
})();
