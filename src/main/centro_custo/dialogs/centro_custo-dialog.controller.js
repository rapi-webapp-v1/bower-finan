(function () {
    'use strict';

    angular
            .module('main.centro_custo')
            .controller('CentroCustoDialogController', CentroCustoDialogController);

    /** @ngInject */
    function CentroCustoDialogController($mdDialog, CentroCusto, row, archive, pai) {

        var vm = this;

        //Data
        vm.isNew = true;
        vm.row = {
            'pai_id': pai ? pai : null,
            'status': true
        };
        vm.title = null;
        vm.centros = [];

        //Methods
        vm.closeDialog = closeDialog;
        vm.update = update;
        vm.save = save;

        activate();

        //-----------------------------------
        function activate()
        {
            archive = archive || [];
            loadCentros();

            if (row && row.id) {
                vm.row = angular.copy(row);
                vm.isNew = false;
            }
        }

        function update()
        {
            CentroCusto.update(row.id, vm.row).then(function (result) {
                if (result) {
                    angular.extend(row, result);
                    closeDialog(row);
                }
            });
        }

        function setFilho(result)
        {
            angular.forEach(archive, function (row) {
                if (result.pai_id === row.id) {
                    if (!row.filhos) {
                        row.filhos = {data: []};
                    }
                    row.filhos.data.push(result);
                }
            });
        }

        function save()
        {
            CentroCusto.save(vm.row).then(function (result) {

                if (!result)
                    return false;


                if (result.pai_id) {
                    setFilho(result);
                } else {
                    archive.push(result);
                    vm.centros.push(result);
                }

                if (vm.continue) {
                    vm.row = {
                        'pai_id': result.pai_id,
                        'status': true
                    };
                } else {
                    closeDialog(result);
                }

            });
        }

        function closeDialog(params)
        {
            $mdDialog.hide(params);
        }

        function loadCentros()
        {
            CentroCusto.search({
                'sort': 'nome',
                'filter': 'onlyPai'
            }).then(function (result) {
                vm.centros = result;
            });
        }

    }


})();
