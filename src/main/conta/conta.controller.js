(function () {
    'use strict';

    angular
            .module('main.conta')
            .controller('ContaController', ContaController);

    /** @ngInject */
    function ContaController(Conta, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'nome', //EX: nome,-created_at
            'q' : null,
            'include' : 'tipo'
        };

        // Methods
        vm.showCreate    = showCreate;
        vm.showEdit      = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search        = search;
        vm.setPage       = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Conta.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Conta.pagination;
            });

        }

        function setPage(p)
        {
            Conta.setPage(p);
            activate();
        }

        function search()
        {
            Conta.setPage(1);
            activate();
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            Conta.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            Conta.dialog.edit(row, vm.archive, ev);
        }

       function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete('Apagar conta', $translate.instant('CONTA.MSG_CONFIRM_DELETE') + '  '+ item.nome, $event)
                    .then(function () {
                        Conta.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }


})();
