(function () {
    'use strict';

    angular
            .module('main.conta')
            .factory('Conta', contaService);


    /** @ngInject */
    function contaService($mdDialog, w3Resource, FinanPaths, $http, w3Utils, w3Date) {
        var urlApi = '/rapi/finan/contas';
        var path_pack = FinanPaths.BASE_PATH;
        var service = {
            querySearch:querySearch,
            saldo:saldo,
            dialog: {
                create: showCreateDialog,
                edit: showDialog
            }
        };

        return w3Resource.extend(urlApi, service);

        // ///////////////////////////////

        function querySearch(query)
        {
            var params = {
                q: query,
                take: 10
            };

            return this.search(params);
        }

        function saldo(conta, dtEnd)
        {
            var data = {
                params: {
                    date_end: w3Date.toFormatEn(dtEnd)
                }
            };
            
            return $http.get(w3Utils.urlApi(urlApi + '/{id}/saldo', conta), data).then(function (result) {
                return result.data.data;
            });
        }

        // DIALOGS

        /**
         * archive: array com a lista de itens
         * data: pode ser undfined, porem se passsar um objeto ja vai abrir o dialog com dados preenchidos, ideal para passar includes tmb
         * */
        function showCreateDialog(archive, ev, data) {
            return showDialog(data, archive, ev);
        }

        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(row, archive, ev) {

            return $mdDialog.show({
                controller: 'ContaDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'conta/dialogs/conta-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row,
                    'archive': archive
                }

            });

        }

    }

})();
