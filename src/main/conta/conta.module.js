(function ()
{
    'use strict';

    angular
            .module('main.conta', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;

        // State
        $stateProvider
                .state('xsession.conta', {
                    url: '/contas',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack +'conta/conta-archive.html',
                            controller: 'ContaController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('configuracoes.conta', {
            title: 'CONTA.MENU',
            icon: 'icon-cash-usd',
            state: 'xsession.conta',
            weight: 25
        });

    }
})();
