(function () {
    'use strict';

    angular
            .module('main.conta')
            .controller('ContaDialogController', ContaDialogController);

    /** @ngInject */
    function ContaDialogController($mdDialog, Conta, row, archive, TipoConta) {

        var vm = this;

        //Data
        vm.isNew = true;
        vm.row = {saldo_inicial: 0};
        vm.title = null;
        vm.tipos = [];

        //Methods
        vm.closeDialog = closeDialog;
        vm.update = update;
        vm.save = save;

        activate();
        //-----------------------------------

        function activate() {

            archive = archive || [];

            if (row && row.id) {
                vm.row = angular.copy(row);
                vm.isNew = false;
            }
            loadTipos();
        }

        function update() {
            vm.row.include = 'tipo';
            Conta.update(row.id, vm.row).then(function (result) {
                if (result) {
                    angular.extend(row, result);
                    closeDialog(row);
                }
            });
        }

        function save() {
            vm.row.include = 'tipo';
            Conta.save(vm.row).then(function (result) {
                if (result) {

                    archive.push(result);
                    closeDialog(result);
                }
            });
        }

        function closeDialog(params) {
            $mdDialog.hide(params);
        }

        function loadTipos() {
            TipoConta.all().then(function (result) {
                vm.tipos = result;

            });
        }

    }


})();
