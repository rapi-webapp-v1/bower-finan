(function ()
{
    'use strict';
    angular
            .module('main.dashboards')
            .controller('DashboardProjectController', DashboardProjectController);

    /** @ngInject */
    function DashboardProjectController( $http, w3Utils)
    {
        //DATA
        var vm = this;
        vm.report = [];
        vm.w6DataDespesa = [];
        vm.w6DataReceita = [];

        vm.widget5 = {
            title: 'Receitas x Despesas',
            mainChart: {
                config: {
                    refreshDataOnly: true,
                    deepWatchData: true
                },
                options: {
                    chart: {
                        type: 'multiBarChart',
                        color: ['#F44336', '#4CAF50'],
                        height: 380,
                        margin: {
                            top: 50,
                            right: 16,
                            bottom: 32,
                            left: 32
                        },
                        clipEdge: true,
                        groupSpacing: 0.1,
                        reduceXTicks: false,
                        stacked: false,
                        duration: 250,
                        x: function (d)
                        {
                            return d.x;
                        },
                        y: function (d)
                        {
                            return d.y;
                        },
                        yAxis: {
                            tickFormat: function (d)
                            {
                                return d;
                            }
                        },
                        legend: {
                            margin: {
                                top: 8,
                                bottom: 32
                            }
                        },
                        controls: {
                            margin: {
                                top: 8,
                                bottom: 0
                            }
                        },
                        tooltip: {
                            gravity: 's',
                            classes: 'gravity-s'
                        }
                    }
                },
                data: []
            },
            days: ['Mon', 'Tue'],
            init: function ()
            {
                vm.w5Data = [
                    {
                        key: 'Despesas',
                        values: [
                            {
                                'key': "Despesas",
                                'series': 0,
                                'size': vm.report.balancete_tri[2].despesa,
                                'x': vm.report.balancete_tri[2].label,
                                'y': vm.report.balancete_tri[2].despesa,
                                'y0': 0,
                                'y1': vm.report.balancete_tri[2].despesa
                            },
                            {
                                'key': "Despesas",
                                'series': 0,
                                'size': vm.report.balancete_tri[1].despesa,
                                'x': vm.report.balancete_tri[1].label,
                                'y': vm.report.balancete_tri[1].despesa,
                                'y0': 0,
                                'y1': vm.report.balancete_tri[1].despesa
                            },
                            {
                                'key': "Despesas",
                                'series': 0,
                                'size': vm.report.balancete_tri[0].despesa,
                                'x': vm.report.balancete_tri[0].label,
                                'y': vm.report.balancete_tri[0].despesa,
                                'y0': 0,
                                'y1': vm.report.balancete_tri[0].despesa
                            }

                        ]
                    },
                    {
                        key: 'Receitas',
                        values: [
                            {
                                'key': "Receitas",
                                'series': 1,
                                'size': vm.report.balancete_tri[2].receita,
                                'x': vm.report.balancete_tri[2].label,
                                'y': vm.report.balancete_tri[2].receita,
                                'y0': 0,
                                'y1': vm.report.balancete_tri[2].receita
                            },
                            {
                                'key': "Receitas",
                                'series': 1,
                                'size': vm.report.balancete_tri[1].receita,
                                'x': vm.report.balancete_tri[1].label,
                                'y': vm.report.balancete_tri[1].receita,
                                'y0': 0,
                                'y1': vm.report.balancete_tri[1].receita
                            },
                            {
                                'key': "Receitas",
                                'series': 1,
                                'size': vm.report.balancete_tri[0].receita,
                                'x': vm.report.balancete_tri[0].label,
                                'y': vm.report.balancete_tri[0].receita,
                                'y0': 0,
                                'y1': vm.report.balancete_tri[0].receita
                            }
                        ]
                    }
                ];
            }
        };

        vm.widget6 = {
            title: 'Programação: Previsto x Realizado',
            mainChart: {
                config: {
                    refreshDataOnly: true,
                    deepWatchData: true
                },
                options: {
                    chart: {
                        type: 'pieChart',
                        color: ['#f44336', '#4CAF50'],
                        height: 315,
                        margin: {
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0
                        },
                        donut: true,
                        clipEdge: true,
                        cornerRadius: 0,
                        labelType: 'percent',
                        padAngle: 0.02,
                        x: function (d)
                        {
                            return d.label;
                        },
                        y: function (d)
                        {
                            return d.value;
                        },
                        tooltip: {
                            gravity: 's',
                            classes: 'gravity-s'
                        }
                    }
                },
                data: []
            },
            init: function ()
            {

                vm.w6DataDespesa = [
                    {
                        label: 'Falta',
                        value: vm.report.eva_programacao.despesa.falta
                    },
                    {
                        label: 'Realizado',
                        value: vm.report.eva_programacao.despesa.realizado
                    }
                ];
                vm.w6DataReceita = [
                    {
                        label: 'Falta',
                        value: vm.report.eva_programacao.receita.falta
                    },
                    {
                        label: 'Realizado',
                        value: vm.report.eva_programacao.receita.realizado
                    }
                ];

            }
        };


        // METHODS

        activate();
        ///////////////////////////
        function activate()
        {
            loadResource();


        }

        function loadResource() {
            var urlApi = '/rapi/finan/reports/dashboard';
            $http.get(w3Utils.urlApi(urlApi)).then(function (result) {
                vm.report = result.data.data;

                vm.widget5.init();
                vm.widget6.init();
            });
        }
    }

})();
