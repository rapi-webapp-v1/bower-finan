(function() {
    'use strict';

    angular
        .module('main.dashboards', [
            // 3rd Party Dependencies
            'nvd3',
            'datatables',
            'chart.js'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths) {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider.state('xsession.dashboards', {
            url: '/dashboard',
            checkSessionRequest: true,
            views: {
                'content@app': {
                    templateUrl: path_pack + 'dashboards/dashboard-project.html',
                    controller: 'DashboardProjectController as vm'
                }
            },
            bodyClass: 'dashboard-project'
        });

        msNavigationServiceProvider.saveItem('dashboards', {
            title: "DASHBOARD.TITLE_MENU",
            icon: 'icon-view-dashboard',
            state: 'xsession.dashboards'
        });

    }

})();
