(function () {
    'use strict';

    angular
            .module('main.encargo')
            .controller('EncargoController', EncargoController);

    /** @ngInject */
    function EncargoController(MetaLoadHelper, xSession) {
        var vm = this;
        var $metas;
        var defaultRow = {
            'multa_tipo': 'fixo',
            'multa': 1.65,
            'juros_tipo': 'percentual',
            'juros_freq': 'diario',
            'juros': 0.033
        };

        // Data
        vm.metaEncagos = {};


        // Methods
        vm.configurarEncargo = configurarEncargo;
        vm.clearForm = clearForm;

        activate();

        //-----------------------

        function activate()
        {
          
            vm.metaEncagos = angular.copy(defaultRow);

            MetaLoadHelper('Rapi\\Finan\\Livro\\Livro', xSession.ID()).then(function (meta) {
                $metas = meta;
                vm.metaEncagos = $metas.fetch('encargos', 'finan', defaultRow);
               
            });

        }

        function configurarEncargo()
        {
            console.log(vm.metaEncagos);
            vm.metaEncagos.$save();
        }

        function clearForm()
        {

            vm.metaEncagos = angular.copy(defaultRow);

        }

    }

})();
