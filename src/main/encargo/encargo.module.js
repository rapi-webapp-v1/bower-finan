(function ()
{
    'use strict';

    angular
            .module('main.encargo', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
                .state('xsession.encargo', {
                    url: '/encargos',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'encargo/encargo.html',
                            controller: 'EncargoController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('configuracoes.encargo', {
            title: 'Encargos',
            icon: 'icon-percent',
            state: 'xsession.encargo',
            weight: 25
        });

    }
})();
