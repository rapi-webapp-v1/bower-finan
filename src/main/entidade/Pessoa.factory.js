(function () {
    'use strict';

    angular
            .module('main.entidade')
            .factory('Pessoa', pessoaFactory);


    /** @ngInject */
    function pessoaFactory(w3Resource) {
        var urlApi = '/rapi/finan/entidades/pessoas';
        var service = {};

        return service = w3Resource.extend(urlApi, service);

        // ///////////////////////////////


    }

})();
