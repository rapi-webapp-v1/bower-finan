(function () {
    'use strict';

    angular
            .module('main.entidade')
            .controller('EntidadeDialogController', EntidadeDialogController);

    /** @ngInject */
    function EntidadeDialogController($mdDialog, $state, Entidade, archive) {

        var vm = this;

        //Data
        vm.row = {nome: ''};
        vm.selectedEntidade = null;
        vm.entidade = null;

        vm.favorecidos = [];
        vm.searched = false;

        //Methods
        vm.closeDialog = closeDialog;
        vm.querySearchEntidade = querySearchEntidade;
        vm.novoFavorecido = novoFavorecido;
        vm.checkId = checkId;

        activate();
        //-----------------------------------

        function activate()
        {
            archive = archive || [];
        }

        function querySearchEntidade(query)
        {
            var params = {
                q: query,
                take: -1,
                include: 'city,item'
//                tipo: vm.tipo
            };
            return Entidade.searchKey(params).then(function (result) {
                vm.favorecidos = result;

                vm.searched = true;
            });

        }

        function checkId(item)
        {

            Entidade.resolveID(item).then(function (id) {
                $state.go('xsession.entidade.edit', {id: id});
                closeDialog();
            });

        }


        function closeDialog(params)
        {
            $mdDialog.hide(params);
        }

        function novoFavorecido(nome)
        {
            var data = {
                'nome': nome,
                'tipo': 'AMBOS',
                'include': 'item'
            };

            Entidade.save(data).then(function (result) {
//                archive.push(result);
                $state.go('xsession.entidade.edit', {id: result.id});
                closeDialog();
            });
        }

    }


})();
