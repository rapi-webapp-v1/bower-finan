(function () {
    'use strict';

    angular
            .module('main.entidade')
            .controller('EntidadeController', EntidadeController)
            .controller('EntidadeFormController', EntidadeFormController);

    /** @ngInject */
    function EntidadeController($translate, $log, $state, Entidade) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.searchActive = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'id', //EX: nome,-created_at
            'q': null,
            'include': 'item',
            'tipo': null
        };


        // Methods
        vm.showCreate = showCreate;
        vm.confirmDelete = confirmDelete;
        vm.search = search;
        vm.setPage = setPage;
        vm.clearFilter = clearFilter;

        activate();

        //-----------------------

        function activate()
        {
            vm.loading = true;
            vm.archive = [];

            Entidade.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Entidade.pagination;
            });

        }

        function setPage(p)
        {
            Entidade.setPage(p);
            activate();
        }

        function search()
        {
            Entidade.setPage(1);
            activate();
        }

        function clearFilter()
        {
            vm.params = {
                'paginate': 10,
                'sort': 'id', //EX: nome,-created_at
                'q': null,
                'include': 'item.city',
                'tipo': null
            };
            vm.searchActive = false;
            search();

        }
        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev)
        {
            Entidade.dialog.create(vm.archive, ev).then(function (result) {
                if (result) {
                    $state.go('xsession.entidade.edit', {id: result.id});
                }
            });
        }


        function confirmDelete(item, $index, $event)
        {
            $log.debug('EntidadeController:confirmDelete', item);

            Entidade.dialog.confirmDelete(item, $event).then(function (isDeleted) {
                if(isDeleted){
                    //vm.archive.splice($index, 1);
                    activate();
                }
            });
        }

    }


    /** @ngInject */
    function EntidadeFormController($log, $stateParams, $state, $filter, Entidade, w3Utils, w3Date, Variables, City) {


        var vm = this;
        var ID;
        vm.tipo = null;
        vm.configUpload = {
            gallery: 'profiles'
        };

        vm.save = save;
        vm.confirmDelete = confirmDelete;
        vm.close = close;

        vm.loadCityWithCEP = loadCityWithCEP;
        vm.estados_civis = Variables.tipo_estado_civil;
        activate();
        //-------------------

        function activate()
        {
            vm.isPopUp = $state.includes('popup');

            if ($stateParams.id) {
                loadResource($stateParams.id);
            } else {
                vm.isNew = true;
            }
        }

        function close()
        {
            window.close();
        }

        function loadResource(id)
        {
            Entidade.find(id, 'city.state.country,token_morph,item').then(function (result) {
                vm.row = result.item;
                vm.row.tipo = result.tipo;
                enableEdit(result.id);
            });
        }

        function enableEdit(id)
        {
            vm.isNew = false;
            ID = id;
        }

        function save()
        {
            $log.info(vm.row);

            if ($stateParams.id) {

                Entidade.update(ID, vm.row).then(function (result) {
                    angular.extend(vm.row, result);

                });


            } else {

                vm.row.tipo_cad = vm.tipo;
                vm.row.status = true;

                Entidade.save(vm.row).then(function (result) {
                    if (result) {
                        $state.go('xsession.entidade.edit', {id: result.id});
                    }
                });

            }
        }

        function confirmDelete($event)
        {
            w3Utils.confirmDelete('Excluir favorecido', vm.row.nome, $event)
                    .then(function () {
                        Entidade.remove(ID).then(goArchive);
                    });

        }

        function goArchive()
        {
            $state.go('xsession.entidade');
        }

        function loadCityWithCEP()
        {
            City.searchCEP(vm.row.cep).then(function (result) {
                if (result.city) {
                    vm.row.city = result.city;
                    vm.row.city_id = result.city.id;
                    vm.row.bairro = vm.row.bairro ? vm.row.bairro : result.bairro;
                    vm.row.logradouro = vm.row.logradouro ? vm.row.logradouro : result.logradouro;
                }
            });
        }


    }



})();
