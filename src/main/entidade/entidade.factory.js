(function () {
    'use strict';

    angular
            .module('main.entidade')
            .factory('Entidade', entidadeService);

    /** @ngInject */
    function entidadeService($mdDialog, $q, w3Resource, w3Presenter, $http, w3Utils, w3Date, FinanPaths) {

        var urlApi = '/rapi/finan/entidades';
        var path_pack = FinanPaths.BASE_PATH;
        var Presenter = w3Presenter.make(responsePresenter, requestPresenter);

        var service = {
            getHash: getHash,
            searchKey: searchKey,
            resolveID: resolveID,
            querySearch: querySearch,
            openPopUpEdit: openPopUpEdit,
            dialog: {
                create: showCreateDialog,
                confirmDelete:confirmDelete
            }
        };

        return service = w3Resource.extend(urlApi, service, Presenter);

        // ///////////////////////////////
        function responsePresenter(data)
        {
            if (data.item) {
                data.item.data_nasc = w3Date.enToObj(data.item.data_nasc);
                data.item.data_cadastro = w3Date.enToObj(data.item.data_cadastro);
            }
            return data;
        }

        function requestPresenter(data)
        {
            data.data_nasc = w3Date.dateToEn(data.data_nasc);
            data.data_cadastro = w3Date.dateToEn(data.data_cadastro);
            return data;
        }

        function showCreateDialog(archive, ev, tipo, data) {
            return showDialog(data, tipo, archive, ev);
        }


        function showDialog(row, archive, ev) {

            return $mdDialog.show({
                controller: 'EntidadeDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'entidade/dialogs/entidade-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row,
                    'archive': archive
                }

            });

        }

        function getHash(hash)
        {
         
            return $http.get(w3Utils.urlApi(urlApi + '/key/{hash}', {hash: hash})).then(function (result) {
                return result.data.data;
            });
        }

        function searchKey(params)
        {
            
            var data = {
                params: params
            };

            return $http.get(w3Utils.urlApi(urlApi + '/key-search'), data).then(function (result) {
                return result.data.data;
            });
        }

        function querySearch(query)
        {

            var data = {
                params : {
                    q: query,
                    take: 10,
                    include: 'item'
                }
            };

//            return search(params);
            return $http.get(w3Utils.urlApi(urlApi), data).then(function (result) {
                return result.data.data;
            });
        }

        /**
         * Passo o obj do search aqui ele retorna o ID
         * @param obj entidade
         * @returns {undefined}
         */
        function resolveID(entidade)
        {
            var deferred = $q.defer();

            if (entidade.id) {

                deferred.resolve(entidade.id);

            } else if (entidade.hash) {

                getHash(entidade.hash).then(function (result) {
                    deferred.resolve(result.id);
                });

            } else {
                deferred.reject('Não encontrado nem ID e nem HASH');
            }

            return deferred.promise;
        }

        function openPopUpEdit(id)
        {
            w3Utils.openPopUp('popup.entidade_edit', {'id': id});
        }

        function confirmDelete(item, $event)
        {
            var deferred = $q.defer();

            w3Utils.confirmDelete('Excluir favorecido', 'Deseja excluir o favorecido ' + item.item.nome + '?', $event)
                .then(function() {
                    service.remove(item.id).then(function() {
                        alertOtherTable(item);
                        deferred.resolve(true);
                    }, function() {
                        deferred.resolve(false);
                    });
                }, function() {
                    deferred.resolve(false);
                });

            return deferred.promise;
        }

        function alertOtherTable(item)
        {
            if (item.table !== 'fn_pessoas') {
                w3Utils.alert('O favorecido foi desvinculado do financeiro! Mas caso queira eliminar o cadastro por completo é necessário ir até o cadastro de <b>' + item.lbTable + '</b>!');
            }
        }


    }



})();
