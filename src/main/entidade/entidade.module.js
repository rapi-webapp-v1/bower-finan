(function ()
{
    'use strict';

    angular
            .module('main.entidade', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
                .state('xsession.entidade', {
                    url: '/favorecidos',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'entidade/entidade-archive.html',
                            controller: 'EntidadeController as vm'
                        }
                    }
                })
                .state('xsession.entidade.create', {
                    url: '/novo',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'entidade/entidade-form.html',
                            controller: 'EntidadeFormController as vm'
                        }
                    }
                })
                .state('xsession.entidade.edit', {
                    url: '/{id}/editar',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'entidade/entidade-form.html',
                            controller: 'EntidadeFormController as vm'
                        }
                    }
                });

        //STATE POPUP
        $stateProvider
                .state('popup.entidade_edit', {
                    url: '/entidades/{id}/editar',
                    views: {
                        'content@popup': {
                            templateUrl: path_pack + 'entidade/entidade-form.html',
                            controller: 'EntidadeFormController as vm'
                        }
                    }
                    
                });
//                .state('popup.favorecido', {
//                    url: '/favorecido?operacao&nome',
//                    views: {
//                        'content@popup': {
//                            templateUrl: path_pack + 'entidade/dialogs/_miolo-favorecido.html',
//                            controller: 'favorecidoDialogController as vm'
//                        }
//                    }
//                });
        msNavigationServiceProvider.saveItem('entidade', {
            title: 'ENTIDADE.MENU',
            icon: 'icon-account-circle',
            state: 'xsession.entidade',
            weight: 30
        });



    }
})();
