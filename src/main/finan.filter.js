(function() {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('rapi.finan')
        .filter('rfFirstUpper', rfFirstUpperFilter)
        .filter('rfFraseEncargos', rfFraseEncargos)
        .filter('rfFixoOuPecent', rfFixoOuPecent);

    /**
     * @Usage: {{vm.row.frequencia | rfFirstUpperFilter}}
     * @output: M || D || S
     */
    function rfFirstUpperFilter() {
        return function(input) {

            if (!input)
                return null;

            input = input.toString();
            return input.substr(0, 1).toUpperCase();
        };
    }

    /**
     * @Usage: {{vm.row.options|rfFraseEncargos}}
     * @output: Multa: .0 % . Juros: .10 % . Ao mês.* O calculo mensal é feito por pró-rata utilizando juros simples.
     */
    /** @ngInject */
    function rfFraseEncargos($filter) {
        return function(options) {
            
            var frase = "";

            if (!angular.isObject(options))
                return null;

            frase += "Multa: " + $filter('rfFixoOuPecent')(options.multa, options.multa_tipo) + " ";
            frase += "Juros: " + $filter('rfFixoOuPecent')(options.juros, options.juros_tipo);

            if (!options.juros_freq)
                return frase + ".";

            //FRQ
            switch (options.juros_freq) {
                case 'mensal':
                    frase += " ao mês";
                    break;
                case 'diario':
                    frase += " ao dia";
                    break;
                case 'manual':
                    frase += " manualmente";
                    break;
                default:
                    frase += " Frq. não indentificada.";
            }

            return frase + ".";
        };

    }

    /**
     * @Usage: {{vm.row.options.juros | rfFixoOuPecent:vm.row.options.juros_tipo}}
     * @output: R$ 5,00 || 5,00%
     */
    /** @ngInject */
    function rfFixoOuPecent($filter) {
        return function(input, tipo) {

            input = parseFloat(input);

            if(!input)
                return "0.00";

            if (tipo === 'percentual') {
                return input + "%";
            } else {
                $filter('currency')(input, 'R$ ');
            }

        };
    }

})();
