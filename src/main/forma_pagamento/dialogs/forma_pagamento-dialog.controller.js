(function () {
    'use strict';

    angular
            .module('main.forma_pagamento')
            .controller('FormaPagamentoDialogController', FormaPagamentoDialogController);

    /** @ngInject */
    function FormaPagamentoDialogController($mdDialog, FormaPagamento, row, archive) {

        var vm = this;

        //Data
        vm.isNew = true;
        vm.row = {status: true};
        vm.title = null;

        //Methods
        vm.closeDialog = closeDialog;
        vm.update = update;
        vm.save = save;

        activate();
        //-----------------------------------

        function activate() {

            archive = archive || [];

            angular.extend(vm.row, row);

            if (vm.row && vm.row.id) {
                vm.isNew = false;
            }
        }

        function update() {
            FormaPagamento.update(row.id, vm.row).then(function (result) {
                if (result) {
                    angular.extend(row, result);
                    closeDialog(row);
                }
            });
        }

        function save() {
            FormaPagamento.save(vm.row).then(function (result) {
                if (result) {
                    archive.push(result);
                    closeDialog(result);
                }
            });
        }

        function closeDialog(params) {
            $mdDialog.hide(params);
        }

    }


})();
