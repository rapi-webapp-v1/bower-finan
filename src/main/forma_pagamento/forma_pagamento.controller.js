(function () {
    'use strict';

    angular
            .module('main.forma_pagamento')
            .controller('FormaPagamentoController', FormaPagamentoController);

    /** @ngInject */
    function FormaPagamentoController(FormaPagamento, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'nome', //EX: nome,-created_at
            'q' : null
        };

        // Methods
        vm.showCreate    = showCreate;
        vm.showEdit      = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search        = search;
        vm.setPage       = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            FormaPagamento.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = FormaPagamento.pagination;
            });

        }

        function setPage(p)
        {
            FormaPagamento.setPage(p);
            activate();
        }

        function search()
        {
            FormaPagamento.setPage(1);
            activate();
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            FormaPagamento.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            FormaPagamento.dialog.edit(row, vm.archive, ev);
        }

       function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('FORMA_PAGAMENTO.TITLE_CONFIRM_DELETE'), $translate.instant('FORMA_PAGAMENTO.MSG_CONFIRM_DELETE') + item.nome + '?', $event)
                    .then(function () {
                        FormaPagamento.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }


})();
