(function ()
{
    'use strict';

    angular
        .module('main.forma_pagamento', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
            .state('app.forma_pagamento', {
                url    : '/forma_pagamentos',
                views  : {
                    'content@app': {
                        templateUrl: path_pack + 'forma_pagamento/forma_pagamento-archive.html',
                        controller : 'FormaPagamentoController as vm'
                    }
                }
            });

            msNavigationServiceProvider.saveItem('configuracoes.forma_pagamento', {
              title: 'FORMA_PAGAMENTO.MENU',
              icon: 'icon-folder-multiple',
              state: 'app.forma_pagamento',
              weight: 40
            });

    }
})();
