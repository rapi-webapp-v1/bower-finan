(function ()
{
    'use strict';

    angular
            .module('main.lancamento')
            .controller('CancelarLancDialogController', CancelarLancamentoDialogController);

    /** @ngInject */
    function CancelarLancamentoDialogController(row, $mdDialog, Lancamento) {

        var vm = this;

        //Data
        vm.row = row;

        //Methods
        vm.closeDialog = closeDialog;
        vm.cancelarLancamento = cancelarLancamento;

        activate();
        //--------------------------
        function activate()
        {
        }

        function cancelarLancamento() {
            Lancamento.disable(vm.row.id, vm.row).then(function (result) {
                
                angular.extend(row, result);
               
                closeDialog(row);
            });
        }


        function closeDialog(params)
        {
            $mdDialog.hide(params);
        }

    }
})();
