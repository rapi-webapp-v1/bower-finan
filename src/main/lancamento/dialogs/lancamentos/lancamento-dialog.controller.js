(function ()
{
    'use strict';

    angular
            .module('main.lancamento')
            .controller('LancamentoDialogController', LancamentoDialogController);

    /** @ngInject */

    function LancamentoDialogController(row, archive, $mdDialog, Lancamento, Entidade,
            FormaPagamento, CentroCusto, Conta) {

        var vm = this;

        //Data
        vm.row = {
            'data_realizada': null,
            'conta_id': null,
            'operacao': 'R',
            'status': false,
            'dt_emissao': new Date(),
            'dt_competencia': new Date(),
            'dt_compensado': new Date(),
            'dt_prevista': null
        };
        vm.isNew = true;
        vm.documentos = [];
        vm.selectedEntidade = null;
        vm.entidade = null;
        vm.contas = [];
        vm.saldo = 0;

        //Methods
        vm.closeDialog = closeDialog;
        vm.save = save;
        vm.querySearchEntidade = querySearchEntidade;
        vm.setEntidade = setEntidade;
        vm.calcSaldo = calcSaldo;
        vm.saveNovoFavorecido = saveNovoFavorecido;
        vm.onSelectedFavorecido = onSelectedFavorecido;
        vm.editFornecedor = editFornecedor;
        vm.onChangePlano = onChangePlano;
        vm.cancelConfirmDialog = cancelConfirmDialog;
        vm.restaurarLancamento = restaurarLancamento;


        activate();
        //-----------------------------------

        function activate()
        {
            //Mytodo create cache in loads pelo menos in sessionStorange
            loadCentros();
            loadContas();
            loadDocumentos();

            if (row) {
                vm.row = vm.row = angular.copy(row);

                if (row.plano_conta_id) {
                    vm.isNew = false;
                    vm.selectedEntidade = vm.row.entidade;

                }

                calcSaldo();
            }
        }

        function querySearchEntidade(query)
        {
            return Entidade.querySearch(query);
        }

        function update()
        {
            Lancamento.update(row.id, vm.row).then(function (result) {
                if (result) {
                    angular.extend(row, result);
                    closeDialog(row);
                }
            });
        }

        function create()
        {
            vm.row.include = 'entidade,conta,centroCusto,planoConta';
            Lancamento.save(vm.row).then(function (result) {

                if (archive && result) {

                    archive.push(result);
                }
                closeDialog(result);
            });
        }

        function save()
        {
            if (vm.isNew) {
                create();
            } else {
                update();
            }
        }



        function restaurarLancamento(row) {
            Lancamento.restore(row.id).then(function (result) {
                angular.extend(row, result);
            });
        }


        function closeDialog(params)
        {
            $mdDialog.hide(params);
        }

        function setEntidade()
        {
            vm.entidade = vm.selectedEntidade;
        }

        function onChangePlano(plano)
        {
            vm.row.plano_conta_id = (plano) ? plano.id : null;
        }

        function toFloat(valor)
        {
            return parseFloat(valor) || 0;
        }

        function calcSaldo()
        {
            vm.saldo = toFloat(vm.row.valor) + toFloat(vm.row.vl_multa) + toFloat(vm.row.vl_juros) - toFloat(vm.row.vl_desconto);
        }

        function saveNovoFavorecido(nome)
        {
            var data = {
                'nome': nome,
                'tipo': vm.row.operacao === 'R' ? 'CLIENTE' : 'FORNECEDOR',
                'include': 'item'
            };

            Entidade.save(data).then(function (result) {
                vm.row.entidade_id = result.id;
                vm.entidade = result.item;
                vm.entidade.tipo = result.tipo;
                vm.entidade.lbTable = result.lbTable;
                
                vm.selectedEntidade = result;
            });
        }

        function editFornecedor(id)
        {
            Entidade.openPopUpEdit(id);
        }

        // LOADS
        function loadCentros()
        {
            CentroCusto.onlyPai().then(function (result) {
                vm.centros = result;
            });
        }

        function loadContas()
        {
            Conta.all().then(function (result) {
                vm.contas = result;
            });
        }

        /* Formas de pagamento */
        function loadDocumentos()
        {
            FormaPagamento.all().then(function (result) {
                vm.documentos = result;
            });
        }

        function onSelectedFavorecido(entidade)
        {
            Entidade.resolveID(entidade).then(function (id) {
                vm.row.entidade_id = id;
                setEntidade();
            });
        }
        
        function cancelConfirmDialog(row, ev) 
        {
            Lancamento.dialog.cancelConfirm(row, ev);
        }
    }
})();