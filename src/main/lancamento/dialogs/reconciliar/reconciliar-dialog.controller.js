(function ()
{
    'use strict';

    angular
            .module('main.lancamento')
            .controller('ReconciliarLancDialogController', ReconciliarLancamentoDialogController);

    /** @ngInject */
    function ReconciliarLancamentoDialogController($log, $mdDialog, row, Lancamento, archive, Conta, Reconciliar) {

        var vm = this;

        //Data
        vm.row = {
            'conta_id': null,
            'saldo_sistema': 0,
            'saldo': 0,
            'data_final': new Date(),
            'descricao': null
        };
        vm.isItem = false;
        vm.info = {
            error: false,
            message: null,
            diff: 0
        };
        vm.saldoAtual = 0;
        vm.contas = [];

        //Methods
        vm.closeDialog = closeDialog;
        vm.onChangeConta = onChangeConta;
        vm.reconciliar = reconciliar;
        vm.openNewLancamento = openNewLancamento;
        vm.onChangeDtFinal = onChangeDtFinal;

        activate();
        //-----------------------------------

        function activate()
        {
            loadContas();

            if (angular.isObject(row)) {
                vm.isItem = true;
                angular.extend(vm.row, row);
            }

            if(vm.row.conta_id){
                onChangeConta(vm.row.conta_id);
            }
        }

        function refreshSaldo()
        {
            if(!vm.row.conta_id)
                return;

            Conta.saldo(vm.row.conta_id, vm.row.data_final).then(function (result) {
                $log.info('saldo', result);
                vm.saldoAtual = result.saldo;
            });
        }

        function onChangeConta(contaID)
        {
            vm.row.conta_id = contaID;
            refreshSaldo();
        }

        function onChangeDtFinal()
        {
            refreshSaldo();
        }

//        function querySearchPlano(query)
//        {
//            return PlanoConta.querySearch(query);
//        }

        function loadContas()
        {
            Conta.all().then(function (result) {
                vm.contas = result;
            });
        }

        function reconciliar()
        {
            Reconciliar.save(vm.row).then(
                    function (result) {
                        if (result)
                          closeDialog();

                    },
                    function (erro) {
                        if (erro.data.error && erro.data.error.code === 'SALDO_NAO_FECHA') {
                            vm.info.error = true;
                            vm.info.message = erro.data.error.message;
                            vm.info.diff = erro.data.data.saldo_diff;
                            vm.saldoAtual = erro.data.data.saldo;
                        }
                    });
        }

        function closeDialog(params)
        {
            $mdDialog.hide(params);
        }

        function openNewLancamento(ev)
        {

            var data = {
                'historico': "Reconciliação de conta - " + moment().format('DD/MM/YYYY'),
                'valor': (vm.info.diff) ? vm.info.diff : vm.info.diff * -1,
                'dt_competencia': vm.row.data_final,
                'dt_compensado': vm.row.data_final,
                'conta_id': vm.row.conta_id,
                'operacao': vm.info.diff < 0 ? 'D' : 'R',
                'marcador': 'reconciliação'
            };

            Lancamento.dialog.create(data, archive, ev);
        }
    }
})();
