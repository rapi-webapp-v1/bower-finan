(function () {
    'use strict';

    angular
            .module('main.lancamento')
            .factory('Reconciliar', ReconciliarService);


    /** @ngInject */
    function ReconciliarService(w3Resource, w3Date, w3Presenter, SessionRequest) {

        var urlApi = SessionRequest.makeUrlSession('/rapi/finan/livro/{session}/reconciliacoes');
        var Presenter = w3Presenter.make(responsePresenter, requestPresenter);

        //var service = {};

        return w3Resource.make(urlApi, Presenter);

        //-------------------------

        function responsePresenter(response)
        {
            response.data_final = w3Date.enToDate(response.data_final);
            return response;
        }

        function requestPresenter(request)
        {
            request.data_final = w3Date.toFormatEn(request.data_final);
            return request;
        }

        // DIALOGS

    }

})();