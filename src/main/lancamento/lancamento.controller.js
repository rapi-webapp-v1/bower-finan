/* global moment */

(function ()
{
    'use strict';
    angular
            .module('main.lancamento')
            .controller('LancamentoController', LancamentoController);


    /** @ngInject */
    function LancamentoController($mdSidenav, $log, $mdDialog, $document, $q, $filter,
            Conta, FormaPagamento, Lancamento, PlanoConta, Entidade, CentroCusto,
            msUtils, w3Utils, Variables, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;

        var vm = this, archive;
        var defaultParams = {
            'contas_id': null,
            'dt_column': 'dt_compensado',
            'dt_start': null,
            'dt_end': null,
            'q': null,
            'onlyTrashed': null,
            'withTrashed': null,
            'valor': null,
            'formas_pagamento': null,
            'include': 'entidade,conta,centroCusto,planoConta'
        };

        // Data
        vm.path_pack = path_pack;
        vm.planos = [];
        vm.defaultContas = 'Todas as contas';
        vm.lancamentos = [];
        vm.contas = [];
        vm.selectedLancamento = [];
        vm.params = angular.copy(defaultParams);
        vm.situacoes = [];
        vm.selectedContas = [];
        vm.selectedEntidades = [];
        vm.selectedPlanos = [];
        vm.selectedCentrosCusto = [];
        vm.config = {};
        vm.limitTo = 0;
        vm.optionsDate = Variables.dates;

        vm.sidebar = {
            'conta': null,
            'saldo': 0,
            'saldoMesPassado': 0,
            'totalSelected': 0,
            'balancete': {}
        };

        // Methods
        vm.openLancamentoDialog = openLancamentoDialog;
        vm.confirmDelete = confirmDelete;
        vm.confirmMultipleDelete = confirmMultipleDelete;
        vm.toggleSelectLancamento = toggleSelectLancamento;
        vm.deselectLancamento = deselectLancamento;
        vm.selectAllLancamento = selectAllLancamento;
        vm.toggleSidenav = toggleSidenav;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;
        vm.clearFilter = clearFilter;
        vm.setConta = setConta;
        vm.loadContas = loadContas;
        vm.loadFormasPagamento = loadFormasPagamento;
        vm.search = search;
        vm.querySearchConta = querySearchConta;
        vm.querySearchEntidade = querySearchEntidade;
        vm.querySearchPlanos = querySearchPlanos;
        vm.querySearchCentroCusto = querySearchCentroCusto;
        vm.transformChip = transformChip;
        vm.openReconciliarDialog = openReconciliarDialog;
        vm.restoreConfirm = restoreConfirm;
        vm.getAllItens = getAllItens;
        vm.getTrasehdItens = getTrasehdItens;
        vm.getOnlyActiveItens = getOnlyActiveItens;
        vm.cancelConfirmDialog = cancelConfirmDialog;
        vm.loadMore = loadMore;
        vm.print = print;

        activate();

        //--------------------------
        function activate()
        {
            defaultParams.dt_start = moment({day: 1, hour: 13}).toDate();
            defaultParams.dt_end = moment().endOf('month').toDate();

            vm.situacoes = Variables.situacao;

            clearFilter();
            search();
        }

        function clearFilter()
        {
            vm.searchActive = false;
            vm.params = angular.copy(defaultParams);
            vm.selectedContas = [];
            vm.selectedEntidades = [];
            vm.selectedPlanos = [];
            vm.selectedCentrosCusto = [];
        }

        function loadContas()
        {
            if (vm.contas.length)
                return;

            return Conta.all('tipo').then(function (result) {
                $log.info('loadContas', result);
                return vm.contas = result;
            });
        }


        /**
         * Quando selecionado uma conta na side-nav
         */
        function setConta(conta)
        {
            if (conta) {

                vm.sidebar.conta = conta;
                vm.selectedContas = [conta];//add ao chips do filtro principal
            } else {
                vm.sidebar.conta = null;
                vm.selectedContas = [];
            }

            loadSaldoByConta(conta);
            makeNameDefaultContas();
            filteLancamento();
        }

        function loadSaldoByConta(conta)
        {
            if (!conta) {
                vm.sidebar.showSaldoConta = false;
                return;
            }

            Conta.find(conta.id, 'saldo,saldoMesAtual,saldoMesPassado').then(function (result) {
                vm.sidebar.showSaldoConta = true;
                vm.sidebar.saldo = result.saldo;
                vm.sidebar.saldoMesPassado = result.saldoMesPassado;
            });
        }

        function reloadSaldoContaActive()
        {
            if (vm.sidebar.conta) {
                loadSaldoByConta(vm.sidebar.conta);
            }
        }
        function getAllItens() {
            vm.params.onlyTrashed = null;
            vm.params.withTrashed = 1;
            search();
        }

        function getTrasehdItens() {
            vm.params.onlyTrashed = 1;
            vm.params.withTrashed = null;
            search();
        }

        function getOnlyActiveItens() {
            vm.params.onlyTrashed = null;
            vm.params.withTrashed = null;
            search();
        }
        /**
         * Quando alterado algum coisa do formulario esta funcao e chamada
         */
        function search()
        {
            vm.params.conta_id = getIds(vm.selectedContas);
            vm.params.entidade_id = getIds(vm.selectedEntidades);
            vm.params.plano_conta_id = getIds(vm.selectedPlanos);
            vm.params.centro_custo_id = getIds(vm.selectedCentrosCusto);

            loadLancamento();
            makeNameDefaultContas();
        }

        /**
         * Concatena nome das contas do header da tabela
         */
        function makeNameDefaultContas()
        {
            if (vm.selectedContas.length) {
                var nomes = vm.selectedContas.map(function (values) {
                    return values.nome;
                });

                vm.defaultContas = 'Contas: ' + nomes.join(', ');
            }else{
               vm.defaultContas = 'Todas as contas';
            }
        }

        function loadFormasPagamento()
        {
            return FormaPagamento.all().then(function (result) {
                return vm.formas_pagamento = result;
            });
        }

        function loadLancamento()
        {
            vm.lancamentos = [];

            Lancamento.search(vm.params).then(function (result) {
                archive = result;
                filteLancamento();

            });

            reloadSaldoContaActive();
        }

        function filteLancamento()
        {
            //var contaID = vm.sidebar.conta ? vm.sidebar.conta.id : undefined;
            vm.limitTo = 0;
            //vm.lancamentos = $filter('filter')(archive, {'conta_id': contaID});
            vm.lancamentos = archive;
            loadMore();
        }

        function loadMore()
        {
            var ct;

            vm.limitTo += 35;
            ct = vm.lancamentos.length;

            if (ct > 0 && vm.limitTo > ct) {
                vm.limitTo = ct;
            }

            $log.info('loadMore:', vm.limitTo);
        }

        /**
         * Open new contact dialog
         *
         * @param ev
         * @param contact
         */
        function openLancamentoDialog(ev, row)
        {
            Lancamento.dialog.create(row, null, ev).then(function (result) {
                if (result)
                    loadLancamento();
            });
        }

        function confirmDelete(item, $index, $event)
        {
            w3Utils.confirmDelete('Excluir movimentação', 'Esta ação é irreversível, deseja realmente excluir a movimentação ' + item.referencia + '( R$ ' + item.valor + ' ) ?', $event)
                    .then(function () {
                        Lancamento.remove(item.id).then(function () {
                            vm.lancamentos.splice($index, 1);
                            reloadSaldoContaActive();
                        });
                    });
        }

        function confirmMultipleDelete(lista, $event)
        {
            var html = "Esta ação é irreversível, deseja realmente excluir as movimentações selecionadas? <br /> <ul>";

            lista.map(function (o) {
                html += "<li>#" + o.id + " - " + o.historico + " " + "(R$ " + o.vl_final + ")</li>";
            });

            html += "</ul>";

            w3Utils.confirmDelete('Excluir movimentações', html, $event)
                    .then(function () {
                        removeAll(lista);
                    });
        }

        function removeAll(lista)
        {
            var tasks = [];

            angular.forEach(lista, function (value) {
                this.push(Lancamento.remove(value.id));
            }, tasks);

            $q.all(tasks).then(function () {
                loadLancamento();
                vm.selectedLancamento = [];
            });
        }

        /**
         * Toggle selected status of the contact
         *
         * @param lancamento
         * @param event
         */
        function toggleSelectLancamento(lancamento, event)
        {
            var sub_totalSelected = 0;
            var ad_totalSelected = 0;
            if (event)
            {
                event.stopPropagation();
            }

            if (vm.selectedLancamento.indexOf(lancamento) > -1)
            {
                vm.selectedLancamento.splice(vm.selectedLancamento.indexOf(lancamento), 1);
            } else
            {
                vm.selectedLancamento.push(lancamento);
            }

            sub_totalSelected = vm.selectedLancamento.sum('vl_despesa');
            ad_totalSelected = vm.selectedLancamento.sum('vl_receita');
            
            vm.sidebar.totalSelected = ad_totalSelected - sub_totalSelected;
        }



        function restoreConfirm(ev, row)
        {
            var confirm = $mdDialog.confirm()
                    .title('Restaurar lançamento')
                    .textContent('Deseja restaurar o lançamento ' + row.historico)
                    .ariaLabel('Restaurar lançamento')
                    .targetEvent(ev)
                    .ok('Restaurar')
                    .cancel('Voltar');

            $mdDialog.show(confirm).then(function () {
                Lancamento.restore(row.id).then(function (result) {
                    angular.extend(row, result);
                });
            });
        }

        /**
         * Deselect lancamento
         */
        function deselectLancamento()
        {
            vm.selectedLancamento = [];
            vm.sidebar.totalSelected = 0;
        }

        /**
         * Sselect all lancamento
         */
        function selectAllLancamento()
        {
            vm.selectedLancamento = vm.lancamentos;
            vm.sidebar.totalSelected = vm.selectedLancamento.sum('valor');
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

        /*
         * QUERY SEARCHS
         */

        // Search contas bancárias
        function querySearchConta(query)
        {
            return Conta.querySearch(query);
        }

        // Search favorecidos/entidades
        function querySearchEntidade(query)
        {
            return Entidade.querySearch(query);
        }

        // Search planos de conta
        function querySearchPlanos(query)
        {
            return PlanoConta.querySearch(query);
        }

        // Search centros de custo
        function querySearchCentroCusto(query)
        {
            return CentroCusto.querySearch(query);
        }

        /*CHIPS PROFISSAO METHODS */
        function transformChip(chip)
        {
            if (angular.isObject(chip)) {
                return chip;
            }

            return {
                name: chip
            };
        }

        /*
         * Responsável por pegar os is dos chips
         */
        function getIds(colection)
        {
            if (!angular.isArray(colection))
                return null;

            return colection.map(function (row) {
                return row.id;
            }).join('|');
        }

        /*
         * RECONCILIAR METHODS
         */
        function openReconciliarDialog(ev, movimentacao)
        {
            $log.info('openReconciliarDialog', movimentacao);

            var row = {};

            if (movimentacao) {
                row.conta_id = movimentacao.conta_id;
                row.data_final = movimentacao.dt_compensado;
            } else {
                row.conta_id = (vm.sidebar.conta) ? vm.sidebar.conta.id : null;
            }

            $mdDialog.show({
                controller: 'ReconciliarLancDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'lancamento/dialogs/reconciliar/reconciliar-dialog.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row,
                    'archive': vm.lancamentos
                }
            }).then(loadLancamento);

        }

        function cancelConfirmDialog(row, ev)
        {
            Lancamento.dialog.cancelConfirm(row, ev);
        }

        function print()
        {
            alert('IMPRIMIR');
        }
    }

})();
