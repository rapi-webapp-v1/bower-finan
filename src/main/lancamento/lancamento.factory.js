(function () {
    'use strict';

    angular
            .module('main.lancamento')
            .factory('Lancamento', LancamentoService);


    /** @ngInject */
    function LancamentoService(w3Resource, w3Date, w3Presenter, $mdDialog, $document, $http, w3Utils, FinanPaths) {

        var urlApi = '/rapi/finan/movimentacao/lancamentos';
        var Presenter = w3Presenter.make(responsePresenter, requestPresenter, paramsPresenter);
        var path_pack = FinanPaths.BASE_PATH;
        var service = {
            disable: disable,
            restore: restore,
            dialog: {
                create: showDialog,
                cancelConfirm: cancelDialog
            }
        };

//        return w3Resource.make(urlApi, Presenter);
        return w3Resource.extend(urlApi, service, Presenter);

        //-------------------------

        function responsePresenter(response)
        {
            response.vl_final = response.operacao === 'D' ? response.vl_despesa * -1 : response.vl_receita;
            response.dt_prevista = w3Date.enToDate(response.dt_prevista);
            response.data_realizada = w3Date.enToDate(response.data_realizada);
            response.dt_competencia = w3Date.enToDate(response.dt_competencia);
            response.dt_compensado = w3Date.enToDate(response.dt_compensado);
            response.dt_emissao = w3Date.enToDate(response.dt_emissao);
            return response;
        }

        function requestPresenter(request)
        {
            request.dt_prevista = w3Date.toFormatEn(request.dt_prevista);
            request.data_realizada = w3Date.toFormatEn(request.data_realizada);
            request.dt_competencia = w3Date.toFormatEn(request.dt_competencia);
            request.dt_compensado = w3Date.toFormatEn(request.dt_compensado);
            request.dt_emissao = w3Date.toFormatEn(request.dt_emissao);

            return request;
        }

        function paramsPresenter(params)
        {
            //Paramentros de filtros
            params.dt_start = w3Date.toFormatEn(params.dt_start);
            params.dt_end = w3Date.toFormatEn(params.dt_end);

            return params;
        }


        function disable(id, data)
        {

            return $http.put(w3Utils.urlApi(urlApi + '/{id}/disable', id), data).then(function (result) {
                return (result.data.status === 'success') ? result.data.data : false;
            });
        }

        function restore(id)
        {
            return $http.put(w3Utils.urlApi(urlApi + '/{id}/restore', id)).then(function (result) {
                return (result.data.status === 'success') ? result.data.data : false;
            });
        }


        // DIALOGS

        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(row, archive, ev)
        {

            return $mdDialog.show({
                controller: 'LancamentoDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'lancamento/dialogs/lancamentos/lancamento-dialog.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    row: row,
                    archive: archive
                }

            });

        }

        function cancelDialog(row, ev)
        {
            return $mdDialog.show({
                controller: 'CancelarLancDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'lancamento/dialogs/cancelamento/cancelar-dialog.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    row: row
                }

            });
        }

    }

})();
