(function ()
{
    'use strict';

    angular
        .module('main.lancamento',[ ] )
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        
        $stateProvider.state('xsession.lancamento', {
            url    : '/lancamentos',
            checkSessionRequest: true,
            views  : {
                'content@app': {
                    templateUrl: path_pack + 'lancamento/lancamento.html',
                    controller : 'LancamentoController as vm'
                }
            }
        });

        // Navigation
        msNavigationServiceProvider.saveItem('lancamento', {
            title : 'Lancamentos',
            icon  : 'icon-repeat',
            state : 'xsession.lancamento',
            weight: 10
        });
    }

})();
