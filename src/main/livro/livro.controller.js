(function () {
    'use strict';

    angular
            .module('main.livro')
            .controller('LivrosController', LivrosController)
            .controller('LivroEditController', LivroEditController);

    /** @ngInject */
    function LivrosController(w3Utils, $translate, Livro) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'nome', //EX: nome,-created_at
            'q': null
        };

        // Methods
        vm.showCreate = showCreate;
        vm.showEdit = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search = search;
        vm.setPage = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Livro.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Livro.pagination;
            });

        }

        function setPage(p)
        {
            Livro.setPage(p);
            activate();
        }

        function search()
        {
            Livro.setPage(1);
            activate();
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            Livro.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            Livro.dialog.edit(row, vm.archive, ev);
        }

        function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete('Apagar conta', $translate.instant('LIVRO.MSG_CONFIRM_DELETE') + '  ' + item.nome, $event)
                    .then(function () {
                        Livro.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }


    }


    /** @ngInject */
    function LivroEditController($stateParams, Livro)
    {
        var vm = this;

        //DATA
        vm.userType = 'Rapi\\Finan\\Livro\\Livro';
        vm.permissions = [
            {
                key: 'admin',
                label: 'Administrativo'
            }
        ];

        //METHODS
        vm.update = update;

        activate();
        //-------------------

        function activate()
        {


            if ($stateParams.id) {
                loadResource($stateParams.id);
            }
        }

        function loadResource(id)
        {
            Livro.find(id, '').then(function (result) {
                console.log(result);
                vm.row = result;

            });
        }

        function update() {
            console.log('salvar');
            Livro.update($stateParams.id, vm.row).then(function (result) {
                if (result) {
                    angular.extend(vm.row, result);
                }
            });
        }
    }
})();
