(function () {
    'use strict';

    angular
            .module('main.livro')
            .factory('Livro', livroService);


    /** @ngInject */
    function livroService($mdDialog, w3Resource, FinanPaths, $http, w3Utils, w3Presenter) {
        var urlApi = '/rapi/finan/livros';
        var Presenter = w3Presenter.make(responsePresenter);
        var path_pack = FinanPaths.BASE_PATH;
        var service = {
            listSessions: listSessions,
            dialog: {
                create: showCreateDialog,
                edit: showDialog
            }
        };

        return w3Resource.extend(urlApi, service);

        // ///////////////////////////////

        // DIALOGS

        /**
         * archive: array com a lista de itens
         * data: pode ser undfined, porem se passsar um objeto ja vai abrir o dialog com dados preenchidos, ideal para passar includes tmb
         * */
        function showCreateDialog(archive, ev, data) {
            return showDialog(data, archive, ev);
        }

        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(row, archive, ev) {

            return $mdDialog.show({
                controller: 'LivroDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'livro/dialogs/livro-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row,
                    'archive': archive
                }

            });

        }

        function listSessions() {

            return $http.get(w3Utils.urlApi(urlApi)).then(function (result) {
                return Presenter.collection(result.data.data);
            });
        }

        function responsePresenter(data) {
            return {
                'id': data.id,
                'name': data.nome,
                'description': data.codigo + data.descricao
            };
        }

    }

})();
