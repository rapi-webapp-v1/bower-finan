(function ()
{
    'use strict';

    angular
            .module('main.livro', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
                .state('app.livro', {
                    url: '/livros',
                
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'livro/livro-archive.html',
                            controller: 'LivrosController as vm'
                        }
                    }
                })
                   .state('app.livro.edit', {
                    url: '/{id}',
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'livro/livro-form.html',
                            controller: 'LivroEditController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('configuracoes.livro', {
            title: 'LIVRO.MENU',
            icon: 'icon-book-open',
            state: 'app.livro',
            weight: 25
        });

    }
})();
