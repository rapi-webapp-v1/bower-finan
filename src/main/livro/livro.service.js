(function () {
    'use strict';

    angular
            .module('main.livro')
            .factory('LivroFinan', livroFactory);


    /** @ngInject */
    function livroFactory(w3Utils) {

        var service = {
            livroId: getLivroId,
            livro: getLivro,
            urlApi: urlApi,
            urlApiLivro: urlApiLivro,
            urlLivro: urlLivro
        };

        return service;
        ////////////////

        function getLivroId() {
            //mytodo configure getLivroId
            return 1;
        }

        function getLivro() {
            //mytodo configure getLivro
            return {
                'nome': "Unidade Master",
                'codigo': "master",
                'descricao': "Livro financeiro da master"
            };
        }

        function urlApi(url, params)
        {
            var urlFinan = '/rapi/finan/' + url;
            return w3Utils.urlApi(urlFinan, params);
        }

        function urlApiLivro(url, params)
        {
            url = urlLivro(url);
            return w3Utils.urlApi(url, params);
        }

        function urlLivro(url)
        {
            return '/rapi/finan/livro/' + getLivroId() + url;
        }



    }
})();
