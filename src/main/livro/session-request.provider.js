(function () {
    'use strict';

    angular
            .module('main.livro')
            .factory('SessionRequest', SessionRequestService);


    /** @ngInject */
    function SessionRequestService($rootScope, $log, $state, w3Utils, w3Local, Variables) {

        var _session = null;

        var config = {
            urlActivate: 'app.home',
            name: Variables.NAME_MODULE
        };

        var service = {
            set: set,
            check: check,
            getId: getSessionId,
            get: getSession,
            urlApi: urlApi,
            urlSession: urlApiSession,
            makeUrlSession: makeUrlSession
        };

        activate();
        return service;
        ////////////////

        function activate(newConfig) {
            config = angular.merge(config, newConfig);
            loadStorange();
            bindEventState();
        }

        function loadStorange()
        {
            var newSession = w3Local.get('SessionRequest.' + config.name);

            if (newSession) {
                set(newSession, false);
            }
        }

        /**
         * Verifica se á uma sessão
         * @return bool
         */
        function check()
        {
            if(_session === null){
                loadStorange();
            }

            return (_session && _session.id > 0) ? true : false;
        }

        function set(newSession, storeSession) {
            _session = newSession;
            $rootScope.currentSession = newSession;

            if (storeSession !== false) {
                w3Local.set('SessionRequest.' + config.name, newSession);
            }
        }

        /**
         * Retorna ID
         * @return int
         */
        function getSessionId() {
            return getSession()['id'];
        }

        /**
         * Retorna o OBJ Inteiro da sessão
         * @return  object
         */
        function getSession() {

            if (check() === false) {
                $log.error('SessionRequest: Empty session request');
                handlerException();
                return false;
            }

            return _session;
        }

        /**
         * Retorna uma URL com paramentros mesmo comportamento do w3Utils.urlApi
         * @param string url
         * @param int|object|null params
         * @return string
         */
        function urlApi(url, params)
        {
            //var urlFinan = '/finan/' + url;
            return w3Utils.urlApi(url, params);
        }

        /**
         * Retorna uma URL com paramentros juntamente com a base da URL + sessionID
         * @param string url
         * @param int|object|null params
         * @return string
         */
        function urlApiSession(url, params)
        {
            //url = getBaseUrlSession(url);//mytodo refatoring getBaseUrlSession is not defined
            return w3Utils.urlApi(url, params);
        }

        /**
         * Gera URL apartir de um parametro
         * EX: /clientes => /finan/livro/1/clientes
         * @param string url
         * @return string
         */
        function makeUrlSession(url)
        {
            return url.replace("{session}", getSessionId());
        }

        function handlerException()
        {
            $state.go(config.urlActivate);
        }

        function bindEventState()
        {
            // Activate loading indicator
            $rootScope.$on('$stateChangeStart', function (event, toState)//event, toState, toParams, fromState, fromParams
            {

                if (toState['checkSessionRequest']===true) {

                    if (check() === false) {
                        $log.info('SessionRequest: handlerException, state => ' . toState.name);
                        event.preventDefault();
                        handlerException();
                    }

                } else {
                    $log.error('ignored state in sessionRequest: ' + toState.name);
                }

            });

        }
    }

})();

//mytodo create route abstract: livro (para registrar o livro ID na URL

/**
 *
 *         <a ui-sref="app.livroSS.demo">demo</a>
 *
        // State
        $stateProvider.state('app.livroSS', {
            //url: '/livro?livro',
            url: '/livro/:livro?',
            abstract: true,
//            views: {
//                'content@app': {
//                    templateUrl: 'app/main/dashboards/dashboard-project.html',
//                    controller: 'DashboardProjectController as vm'
//                }
//            },
//            bodyClass: 'dashboard-project',
            params: {
                livro: {
                    value: '12',
                    squash: true
                }
            }
        });

        $stateProvider.state('app.livroSS.demo', {
             url: '/demo',
            views: {
                'content@app': {
                    template: 'demo',
                    //controller: 'DashboardProjectController as vm'
                }
            },
        });
        $stateProvider.state('app.livroSS.dashboards', {
            url: '/dashboard',
            views: {
                'content@app': {
                    templateUrl: 'app/main/dashboards/dashboard-project.html',
                    controller: 'DashboardProjectController as vm'
                }
            },
            bodyClass: 'dashboard-project'
        });

        //resolver retornando uma funcion
        $stateProvider.state("byPlayer", {
          url : "/player/{id}/{year}",
          params: {
            year: {
              value: function() { return getCurrentYear(); },
              squash: true
            }
          }
        })

 // outra forma de resolver params
 $stateProvider.state("stateTabs.state", {
    url: "/state/:stateNum",
    params: { // default params
       stateNum: "3"               // here belongs stateNum
    }
  });
  $stateProvider.state("stateTabs.state.sub", {
    url: "/sub/:subState",
    params: { // default params
      subState: "x"                // here can be subState
    }
  });


//DECORATE
//REF: http://stackoverflow.com/questions/24972750/angular-ui-router-default-parameter
app.config(['$provide',
    function($provide) {
        $provide.decorator('$state', function($delegate, UserService) {

            // Save off delegate to use 'state' locally
            var state = $delegate;

            // Save off reference to original state.go
            state.baseGo = state.go;

            // Decorate the original 'go' to always plug in the userID
            var go = function(to, params, options) {

                params.userID = UserService.userID;

                // Invoke the original go
                this.baseGo(to, params, options);
            };

            // assign new 'go', decorating the old 'go'
            state.go = go;

            return $delegate;
        });
    }
]);


//USANDO RESOLVER para injetar o livro
//http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
 */
