(function () {
    'use strict';

    angular
            .module('main.plano_conta')
            .controller('PlanoContaDialogController', PlanoContaDialogController);

    /** @ngInject */
    function PlanoContaDialogController($mdDialog, $translate, PlanoConta, archive, pai, row, w3Utils) {

        var vm = this;

        //Data
        vm.isNew = true;
        vm.row = {
            status: true
        };
        vm.pai = angular.copy(pai);
        //vm.title = null;
        vm.nv1 = {};
        vm.nv2 = {};
        vm.nv3 = {};

        //Methods
        vm.closeDialog = closeDialog;
        vm.update = update;
        vm.save = save;
        vm.setPai = setPai;
        vm.confirmDelete = confirmDelete;

        activate();
        //-----------------------------------

        function activate() {
            vm.archive = archive || [];

            if (archive.length) {
                vm.nv1 = archive;//$filter('filter')(archive, {'tipo' : pai.tipo});
                vm.nv2 = {};
            }

            if (row) {
                vm.isNew = false;
                vm.row = angular.copy(row);
                vm.cod = row.codigo.replace(pai.codigo + '.', "");
            }
        }

        function setPai(pai)
        {
            vm.pai = pai;
        }

        function checkClose()
        {
            if (vm.continue) {
                vm.row = {
                    status: true
                };
                vm.cod = '';
                //vm.pai = {}; 
            } else {
                closeDialog(true);
            }
        }

        function update()
        {
            vm.row.pai_id = vm.pai.id;
            vm.row.codigo = vm.cod;

            PlanoConta.update(row.id, vm.row).then(function (result) {
                if (result)
                    checkClose();
            });

        }

        function save() 
        {            
            var data = {};
            
            if (vm.nv2 === 'NOVO') {
                data = {
                    tipo: 'AMBOS',
                    codigo: vm.cod,
                    nome: vm.row.nome,
                    descricao: vm.row.descricao
                };
            } else {
                data = angular.copy(vm.row);
                data.pai_id = vm.pai ? vm.pai.id : null;
                data.codigo = vm.cod;
            }

            PlanoConta.save(data).then(function (result) {
                if (result)
                    checkClose();
            });
        }

        function closeDialog(params) {
            $mdDialog.hide(params);
        }

        function confirmDelete(id, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete('Excluir plano de conta', $translate.instant('PLANO_CONTA.MSG_CONFIRM_DELETE') + '  ' + vm.row.nome, $event)
                    .then(function () {
                        PlanoConta.remove(id);

                        angular.forEach(vm.archive, function (value3) {

                            angular.forEach(value3.filhos.data, function (value, key) {
                                if (value.id == id) {
                                    value3.filhos.data.splice(key, 1);
                                } else {
                                    angular.forEach(value.filhos.data, function (value2, key2) {
                                        if (value2.id == id) {
                                            value.filhos.data.splice(key2, 1);
                                        }
                                    });
                                }
                            });
                        });
                    });

        }

    }


})();
