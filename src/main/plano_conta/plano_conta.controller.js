(function () {
    'use strict';

    angular
            .module('main.plano_conta')
            .controller('PlanoContaController', PlanoContaController);

    /** @ngInject */
    function PlanoContaController(PlanoConta, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.loading = false;
        vm.archive = [];
        vm.pagination = {};
        vm.nivel = null;
        vm.params = {
            'paginate': null,
            'sort': 'codigo', //EX: nome,-created_at
            'q': null,
            'filter': 'onlyPai',
            'include': 'filhos.filhos'
        };

        // Methods
        vm.showCreate = showCreate;
        vm.showEdit = showEdit;
        vm.confirmDelete = confirmDelete;
//        vm.search = search;
        vm.setPage = setPage;
        vm.setUp = setUp;

        activate();

        //-----------------------
        function activate()
        {
            vm.loading = true;
            //vm.archive = null;

            PlanoConta.search(vm.params).then(function (result) {

                vm.loading = false;
                vm.archive = result;

                vm.pagination = PlanoConta.pagination;
            });

        }

        function setUp()
        {
            PlanoConta.setUp().then(function () {
                activate();
            });
        }

        function setPage(p)
        {
            PlanoConta.setPage(p);
            activate();
        }

//        function search()
//        {
//            PlanoConta.setPage(1);
//            activate();
//        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(pai, ev)
        {
            PlanoConta.dialog.create(vm.archive, pai, ev)
                    .then(activate);
        }

        function showEdit(pai, row, ev)
        {
            PlanoConta.dialog.edit(vm.archive, pai, row, ev)
                    .then(activate);
        }

        function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('PLANO_CONTA.MSG_CONFIRM_DELETE'), item.nome, $event)
                    .then(function () {
                        PlanoConta.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }


})();
