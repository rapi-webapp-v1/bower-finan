(function () {
    angular.module('main.plano_conta')
            .directive('rfAcPlanoConta', autoCompletePlanoConta);


    /** @ngInject */
    function autoCompletePlanoConta(PlanoConta, FinanPaths) {
        var path_pack = FinanPaths.BASE_PATH;
        var directive = {
            restrict: 'E',
            scope: {
                'plano': '=plano',
                'tipo': '=tipo',
                'onChangePlano': '&onChangePlano',
                'required': '@acRequired'
            },
            templateUrl: path_pack + 'plano_conta/_ac-plano-conta.html',
            link: link
        };

        return directive;

        function link(scope, $element)
        {
            scope.selectedItem;
            scope.label = '';
            if (scope.plano) {

                scope.selectedItem = scope.plano;
                scope.label = scope.plano.codigo + " - " + scope.plano.nome;

            }


            scope.loadPlanos = function () {

                var params = {
                    'sort': 'codigo', //EX: nome,-created_at

                    'filter': 'onlyPai',
                    'include': 'filhos.filhos'
                };

                PlanoConta.search(params).then(function (result) {

                    if (scope.tipo === 'D') {
                        scope.planos = result[1];

                    } else {
                        scope.planos = result[0];

                    }
                });
            };

            scope.querySearchPlano = function (query)
            {
                return PlanoConta.querySearch(query);
            };

            scope.setPlano = function (plano)
            {
                scope.onChangePlano()(plano);
            };

            scope.clearSearchTerm = function () {
                scope.searchTerm = '';
                scope.setPlano(scope.selectedItem);
            };

            $element.find('input').on('keydown', function (ev) {
                ev.stopPropagation();
            });
        }
    }


})();
