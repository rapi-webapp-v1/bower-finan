(function () {
    'use strict';

    angular
            .module('main.plano_conta')
            .factory('PlanoConta', plano_contaService);


    /** @ngInject */
    function plano_contaService($mdDialog, $http, w3Utils, w3Resource, w3Presenter, FinanPaths) {

        var urlApi = '/rapi/finan/plano-contas';
        var presenter = w3Presenter.make(responsePresenter);
        var path_pack = FinanPaths.BASE_PATH;
        var service = {
            'setUp': setUp,
            'querySearch': querySearch,
            dialog: {
                create: showCreateDialog,
                edit: showDialog
            }
        };

        return w3Resource.extend(urlApi, service, presenter);

        //----------------------------------
        function responsePresenter(data)
        {
            data.nome_codigo = data.codigo + " - " + data.nome;
            return data;
        }

        function setUp()
        {
            return $http.post(w3Utils.urlApi(urlApi + "/setup"), '').then(function (result) {
                return result.data.data;
            });
        }

        function querySearch(query)
        {
            var params = {
                q: query,
                nivel:3,
                sort: 'codigo',
                take: 10
            };

            return this.search(params);
        }

        // DIALOGS

        /**
         * archive: array com a lista de itens
         * data: pode ser undfined, porem se passsar um objeto ja vai abrir o dialog com dados preenchidos, ideal para passar includes tmb
         * */
        function showCreateDialog(archive, pai, ev) {
            return showDialog(archive, pai, null, ev);
        }

        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(archive, pai, row, ev) {

            return $mdDialog.show({
                controller: 'PlanoContaDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'plano_conta/dialogs/plano_conta-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'pai': pai,
                    'row': row,
                    'archive': archive
                }

            });

        }

    }

})();
