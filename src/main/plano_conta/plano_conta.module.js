(function ()
{
    'use strict';

    angular
            .module('main.plano_conta', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
                .state('xsession.plano_conta', {
                    url: '/plano_contas',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'plano_conta/plano_conta-archive.html',
                            controller: 'PlanoContaController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('configuracoes.plano_conta', {
            title: 'PLANO_CONTA.MENU',
            icon: 'icon-file-document-box',
            state: 'xsession.plano_conta',
            weight: 30
        });

    }
})();
