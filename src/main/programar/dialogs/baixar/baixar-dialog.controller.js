(function ()
{
    'use strict';

    angular
            .module('main.programar')
            .controller('BaixarProgDialogController', BaixarProgDialogController);

    /** @ngInject */
    function BaixarProgDialogController(row, $mdDialog, Programar,
            FormaPagamento, CentroCusto, Conta) {

        var vm = this;

        var defaultConf_juros = {
            'multa_tipo': 'fixo',
            'juros_tipo': 'percentual',
            'juros_freq': 'manual',
            'multa': null,
            'juros': null
        };

        //Data
        vm.confJuros = false;
        vm.documentos = [];
        vm.entidade = null;
        vm.contas = [];
        vm.conf_juros = {};
        vm.saldo = 0;
        vm.dtPagamento = null;
        vm.row = {};
        vm.enableJuros = false;

        //Methods
        vm.closeDialog = closeDialog;
        vm.update = update;
        vm.calcSaldo = calcSaldo;
        vm.onChangePlano = onChangePlano;
        vm.toggleconfJuros = toggleconfJuros;
        vm.setJuros = setJuros;
        vm.checkTypeEncargo = checkTypeEncargo;
        vm.baixar = baixar;
        vm.updateEncargos = updateEncargos;
        vm.toggleJuros = toggleJuros;

        activate();
        //-----------------------------------

        function activate()
        {
            //Mytodo create cache in loads pelo menos in sessionStorange
            loadCentros();
            loadContas();
            loadDocumentos();

            vm.conf_juros = vm.row.options ? vm.row.options : defaultConf_juros;

            vm.row = angular.copy(row);

            vm.dtPagamento = vm.row.dt_prevista ? vm.row.dt_prevista : new Date();

            calcSaldo();


        }

        function updateEncargos()
        {
            if(!vm.row.options.juros && !vm.row.options.multa)
                return;

            Programar.updateEncargos(vm.row.id, vm.dtPagamento, vm.row.valor).then(function (result) {
                if (result) {
                    angular.extend(vm.row, result);
                    calcSaldo();
                }
            });
        }

        function baixar()
        {
            vm.row.dt_compensado = vm.dtPagamento;

            Programar.baixarProgramacao(row.id, vm.row).then(function (result) {
                if (result) {
                    angular.extend(row, result);
                    closeDialog(row);
                }
            });
        }

        function closeDialog(params)
        {
            $mdDialog.hide(params);
        }

        function onChangePlano(plano)
        {
            vm.row.plano_conta_id = (plano) ? plano.id : null;
        }

        function calcSaldo()
        {
            vm.saldo = Programar.calcSaldo(vm.row);
        }

        // LOADS
        function loadCentros()
        {
            CentroCusto.onlyPai().then(function (result) {
                vm.centros = result;
            });
        }

        function loadContas()
        {
            Conta.all().then(function (result) {
                vm.contas = result;
            });
        }

        /* Formas de pagamento */
        function loadDocumentos()
        {
            FormaPagamento.all().then(function (result) {
                vm.documentos = result;
            });
        }

        function toggleconfJuros()
        {
            vm.confJuros = !vm.confJuros;
            vm.conf_juros = angular.copy(vm.row.options);
        }

        function toggleJuros()
        {
            vm.row.vl_juros = 0;
            vm.row.vl_multa = 0;
            if(vm.enableJuros){
                vm.row.options.juros_freq = 'manual';

            }else{
                vm.row.options.juros_freq = 'automatico';
            }
        }

        function setJuros()
        {
            vm.confJuros = !vm.confJuros;
            vm.row.options = {
                juros : vm.row.vl_juros,
                juros_freq: 'manual',
                juros_tipo: 'fixo',
                multa: vm.row.vl_multa,
                multa_tipo: 'fixo'
            };
            update();
        }

        //Atuliza programacao
        function update()
        {
            vm.row.dt_compensado = vm.dtPagamento;

            Programar.update(vm.row.id, vm.row).then(function (result) {
                if (result) {
                    angular.extend(vm.row, result);
                    calcSaldo();
                }
            });
        }

        function checkTypeEncargo()
        {
            if (vm.conf_juros.juros_freq === 'manual') {
                vm.conf_juros.juros_tipo = 'fixo';
                vm.conf_juros.multa_tipo = 'fixo';
            }

        }
    }
})();
