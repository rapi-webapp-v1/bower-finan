/* global RRule */

/* global RRule, moment */

(function ()
{
    'use strict';

    angular
            .module('main.programar')
            .controller('ProgramarDialogController', ProgramarDialogController);

    /** @ngInject */
    function ProgramarDialogController(row, $mdDialog,
            Variables, Programar, Entidade, FormaPagamento, CentroCusto, Conta) {

        var vm = this;
        var thismonth = moment().month();

        var confDespesa = {
            'title': 'Conta a pagar',
            'title_edit': 'Editar conta a pagar',
            'icon_font': 'icon-trending-down',
            'bg_color_class': 'red-500-bg',
            'fg_color_class': 'red-500-fg',
            'operacao': 'D',
            'label': 'Pagar para:',
            'tipo_favorecido': 'FORNECEDOR'
        };

        var confReceita = {
            'title': 'Conta a receber',
            'title_edit': 'Editar conta a receber',
            'icon_font': 'icon-trending-up',
            'bg_color_class': 'green-500-bg',
            'fg_color_class': 'green-500-fg',
            'operacao': 'R',
            'label': 'Receber de:',
            'tipo_favorecido': 'CLIENTE'
        };
        var def_options = {

                'multa_tipo': 'fixo',
                'juros_tipo': 'percentual',
                'juros_freq': 'manual'
        };

        //Data
        vm.row = {
            'conta_id': null,
            'operacao': 'R',
            'status': false,
            'dt_emissao': moment().toDate(),
            'dt_competencia': moment().toDate(),
            'dt_prevista': moment().set('month', thismonth + 1).toDate(),
            'frequencia': 'unica'
        };
        vm.continue = false;
        vm.isNew = true;
        vm.openBaixa = false;
        vm.documentos = [];
        vm.frequencia = [];
        vm.selectedEntidade = null;
        vm.entidade = null;
        vm.title = null;
        vm.contas = [];
        vm.saldo = 0;
        vm.parcelamento = [];
        vm.divisao = [];
        vm.ger = {};
        vm.parcelas = [];
        var defaultGer = {
            vezes: null,
            data_parcela: new Date,
            entrada: 0
        };

        //Methods
        vm.closeDialog = closeDialog;
        vm.save = save;
        vm.gerarPagamentos = gerarPagamentos;
        vm.querySearchEntidade = querySearchEntidade;
        vm.onChangePlano = onChangePlano;
        vm.repeatFormaPagamento = repeatFormaPagamento;
        vm.calcSaldo = calcSaldo;
        vm.reCalc = reCalc;
        vm.removeParcela = removeParcela;
        vm.saveNovoFavorecido = saveNovoFavorecido;
        vm.onSelectedFavorecido = onSelectedFavorecido;
        vm.editFornecedor = editFornecedor;
        vm.initParc = initParc;
        vm.loadParcelas = loadParcelas;
        vm.openBaixarProgDialog = openBaixarProgDialog;

        activate();
        //-----------------------------------
        function activate()
        {
            setOperacao(row.operacao);

            //mytodo salvar em essionStorange estes dados de centro/contas/documentos
            loadCentros();
            loadContas();
            loadDocumentos();
            vm.ger = angular.copy(defaultGer);

            if (row && row.id > 0) {
                vm.isNew = false;
                vm.row = angular.copy(row);
                vm.selectedEntidade = vm.row.entidade;
                vm.calcSaldo();
                vm.parcelamento = Programar.transformResponseParcelas(vm.row.parcelas);
            }

            vm.frequencia = Variables.frequencia;
        }

        function setOperacao(operacao)
        {
            if (operacao === 'D') {
                vm.config = confDespesa;
                vm.row.operacao = 'D';
            } else {
                vm.config = confReceita;
                vm.row.operacao = 'R';
            }
        }

        function querySearchEntidade(query)
        {
            return Entidade.querySearch(query);
        }


        function onChangePlano(plano)
        {
            vm.row.plano_conta_id = (plano) ? plano.id : null;

        }

        function update()
        {
            Programar.update(row.id, vm.row).then(function (result) {
                angular.extend(vm.row, result);

                if (vm.openBaixa) {
                    closeDialog(vm.row);
                    openBaixarProgDialog(vm.row);
                } else {
                    closeDialog(vm.row);
                }
            });
        }

        function loadParcelas()
        {

            Programar.search({'pai_id': vm.row.pai_id, 'include': 'formaPagamento,entidade,conta,planoConta'}).then(function (result) {
                vm.parcelas = result;
            });
        }

        function create()
        {
            vm.row.parcelas = angular.copy(vm.parcelamento);
            vm.row.parcelas = Programar.transformRequestParcelas(vm.row.parcelas);

            vm.row.frequencia_rrule = createRepeatRule();

            Programar.save(vm.row).then(function (result) {
                if (vm.continue && result) {
                    _resetForm();
                } else {
                    closeDialog(result);
                }
            });
        }

        function _resetForm()
        {
            vm.selectedEntidade = null;

            vm.row.detalhes = null;
            vm.row.dt_compensado = null;
            vm.row.entidade_id = null;
            vm.row.forma_pagamento_id = null;
            vm.row.frequencia = null;
            vm.row.frequencia_id = null;
            vm.row.historico = null;
            vm.row.id = null;
            vm.row.motivo_cancelamento = null;
            vm.row.n_parcela = null;
            vm.row.qt_parcela = 0;
            vm.row.referencia = null;
            vm.row.valor = 0;
            vm.row.vl_desconto = 0;
            vm.row.vl_despesa = 0;
            vm.row.vl_juros = 0;
            vm.row.vl_multa = 0;
            vm.row.vl_receita = 0;


            vm.ger = angular.copy(defaultGer);
            vm.selectedEntidade = null;
            vm.entidade = null;
            vm.saldo = 0;
            vm.parcelamento = [];
            vm.divisao = [];
            vm.parcelamento = [];

        }

        function createRepeatRule()
        {
            if (vm.row.frequencia === 'unica') {
                return null;
            }

            var rule = new RRule({
                freq: vm.row.frequencia,
                interval: 1,
                dtstart: vm.row.dt_prevista,
                until: vm.row.frequencia_fim
            });

            return rule.toString();
        }

        function save()
        {
            if (vm.isNew) {
                create();
            } else {
                update();
            }
        }

//        function openBaixa()
//        {
//            vm.row.dt_compensado = vm.dt_pagamento;
//
//            Programar.baixarProgramacao(vm.row.id, vm.row).then(function (result) {
//                angular.extend(row, result);
//                closeDialog(row);
//            });
//        }

        function openBaixarProgDialog(row, ev)
        {
            Programar.dialog.dialogBaixarProg(row, ev).then(function (result) {
                if (result) {
//                    loadProgramar();
                }
            });
        }


        function closeDialog(params)
        {
            $mdDialog.hide(params);
        }

        function saveNovoFavorecido(nome)
        {
            var data = {
                'nome': nome,
                'tipo': vm.config.tipo_favorecido,
                'include': 'item'
            };

            Entidade.save(data).then(function (result) {
                vm.row.entidade_id = result.id;
                vm.entidade = result.item;
                vm.entidade.tipo = result.tipo;
                vm.entidade.lbTable = result.lbTable;

                vm.selectedEntidade = result;
            });
        }

        /**
         * md-selected-item-change do autocomplete Entidades
         */
        function onSelectedFavorecido(entidade)
        {
            Entidade.resolveID(entidade).then(function (id) {
                vm.row.entidade_id = id;
                vm.entidade = vm.selectedEntidade;
            });
        }

        function editFornecedor(id)
        {
            Entidade.openPopUpEdit(id);
        }

        /**
         * Quando usuário muda a forma de pagamento de uma parcela, ex: 2/8
         * ele pode clicar no BTN para repetir a mesma opção dali pra frente
         *
         * @param int forma_id
         * @param int i
         */
        function repeatFormaPagamento(forma_id, i)
        {
            i = i || 0;

            angular.forEach(vm.parcelamento, function (values, k) {
                if (i < k)
                    values.forma_pagamento_id = forma_id;
            });
        }

        function gerarPagamentos()
        {
            var valorAparcelar = toFloat(vm.saldo);
            var parcela = (valorAparcelar / vm.ger.vezes).toFixed(2);
            var date = vm.ger.data_parcela ? moment(vm.ger.data_parcela) : moment(new Date());

            var parcelamento = [];

            //Gera restante das parcelas
            for (var i = 0; i < vm.ger.vezes; i++) {

                var obj = {
                    valor: parcela,
                    forma_pagamento_id: vm.ger.forma_pagamento_id,
                    dt_prevista: i === 0 ? date.toDate() : angular.copy(date.add(1, "months").toDate())
                };
                parcelamento.push(obj);
            }

            vm.parcelamento = parcelamento;

            reCalc();
        }


        function removeParcela(item, ind)
        {
            vm.ger.vezes = vm.ger.vezes - 1;
            vm.parcelamento.splice(ind);
            reCalc();
        }

        function reCalc()
        {
            var subtotal = toFloat(vm.saldo);
            var valorParcelas = getValorParcelas();
            vm.valor_restante = subtotal - valorParcelas;
            vm.valor_final = subtotal;
        }

        function toFloat(valor)
        {
            return parseFloat(valor) || 0;
        }

        function initParc()
        {
            if (!vm.ger.data_parcela) {
                vm.ger.data_parcela = vm.row.dt_prevista;
            }
            if (vm.row.forma_pagamento_id) {
                vm.ger.forma_pagamento_id = vm.row.forma_pagamento_id;
            }
        }

        function getValorParcelas()
        {
            var total = 0;
            angular.forEach(vm.parcelamento, function (obj) {
                total += toFloat(obj.valor);
            });
            return total.toFixed(2);
        }

        function calcSaldo()
        {
            vm.saldo = toFloat(vm.row.valor) + toFloat(vm.row.vl_multa) + toFloat(vm.row.vl_juros) - toFloat(vm.row.vl_desconto);
            reCalc();
        }

        function loadCentros()
        {
            CentroCusto.onlyPai().then(function (result) {
                vm.centros = result;
            });
        }

        function loadContas()
        {
            Conta.all().then(function (result) {
                vm.contas = result;
            });
        }

        /* Formas de pagamento */
        function loadDocumentos()
        {
            FormaPagamento.all().then(function (result) {
                vm.documentos = result;
            });
        }


    }
})();
