/* global moment */

(function ()
{
    'use strict';

    angular
            .module('main.programar')
            .controller('ProgramarController', ProgramarController);

    /** @ngInject */
    function ProgramarController($mdSidenav, $log, $filter, $stateParams,
            $state, msUtils, $mdDialog, $document, Conta, FormaPagamento, Programar,
            w3Date, w3Utils, Entidade, PlanoConta, CentroCusto, Variables, $q, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        var vm = this;
        var archive = [];
        var confDespesa = {
            'title': 'Contas a pagar',
            'icon_font': 'icon-trending-down',
            'bg_color_class': 'red-500-bg',
            'fg_color_class': 'red-500-fg',
            'operacao': 'D'
        };
        var confReceita = {
            'title': 'Contas a receber',
            'icon_font': 'icon-trending-up',
            'bg_color_class': 'green-500-bg',
            'fg_color_class': 'green-500-fg',
            'operacao': 'R'
        };
        var defaultParams = {
            'dt_column': 'dt_prevista',
            'dt_start': null,
            'dt_end': null,
            'sort': 'dt_prevista',
            'take': -1,
            'include': 'entidade,conta,planoConta'
        };

        // Data
        vm.filteredProgramar = [];
        vm.config = {};
        vm.contas = [];
        vm.filterIds = null;
        vm.selectedProgramar = [];
        vm.params = angular.copy(defaultParams);
        vm.situacoes = [];
        vm.selectedContas = [];
        vm.selectedEntidades = [];
        vm.selectedPlanos = [];
        vm.selectedCentrosCustos = [];
        vm.planos = [];
        vm.searchActive = false;
        vm.selectedAll = false;
        vm.defaultContas = 'Todas as contas';
        vm.limitTo = 0;
        vm.optionsDate = Variables.dates;
        vm.confQuey = {
            'on': false,
            'title': null
        };
        vm.sidebar = {
            'totalSelected': 0,
            'balancete': {}
        };


        // Methods
        vm.openProgramarDialog = openProgramarDialog;
        vm.confirmDelete = confirmDelete;
        vm.confirmMultipleDelete = confirmMultipleDelete;
        vm.toggleSelectProgramar = toggleSelectProgramar;
        vm.deselectProgramar = deselectProgramar;
        vm.selectAllProgramar = selectAllProgramar;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;
        vm.setOperacao = setOperacao;
        vm.loadFormasPagamento = loadFormasPagamento;
        vm.updateItem = updateItem;
        vm.clearFilter = clearFilter;
        vm.search = search;
        vm.querySearchConta = querySearchConta;
        vm.querySearchEntidade = querySearchEntidade;
        vm.querySearchPlanos = querySearchPlanos;
        vm.querySearchCentroCusto = querySearchCentroCusto;
        vm.transformChip = transformChip;
        vm.toggleSidenav = toggleSidenav;
        vm.searchParcelas = searchParcelas;
        vm.searchRepeats = searchRepeats;
        vm.openBaixarProgDialog = openBaixarProgDialog;
        vm.loadMore = loadMore;
        vm.print = print;


        activate();
        //--------------------------

        function activate()
        {
            setOperacao($stateParams.operacao);
            vm.situacoes = Variables.situacao;

            defaultParams.dt_start = moment({day: 1, hour: 13}).toDate();
            defaultParams.dt_end = moment().endOf('month').toDate();

            //carrega os filtro padrão e faz uma nova requisição
            clearFilter();
        }

        function setOperacao(operacao)
        {
            if (operacao === 'despesa') {
                vm.config = confDespesa;
            } else {
                vm.config = confReceita;
            }

            //altera a url sem recarregar o controle
            $state.go('xsession.programar', {'operacao': operacao}, {notify: false});
            filterProgramar();
        }


        function clearFilter()
        {
            vm.searchActive = false;
            vm.confQuey.on = false;

            vm.params = angular.copy(defaultParams);
            vm.selectedContas = [];
            vm.selectedEntidades = [];
            vm.selectedPlanos = [];
            vm.selectedCentrosCustos = [];

            search();
        }

        function loadMore()
        {
            var ct;

            vm.limitTo += 35;
            ct = vm.filteredProgramar.length;

            if (ct > 0 && vm.limitTo > ct) {
                vm.limitTo = ct;
            }

        }

        /**
         * Quando alterado algum coisa do formulario esta funcao e chamada
         */
        function search()
        {
            vm.params.conta_id = getIds(vm.selectedContas);
            vm.params.entidade_id = getIds(vm.selectedEntidades);
            vm.params.plano_conta_id = getIds(vm.selectedPlanos);
            vm.params.centro_custo_id = getIds(vm.selectedCentrosCustos);

            loadProgramar();
            makeNameDefaultContas();
        }

        function loadProgramar()
        {
            vm.filteredProgramar = [];
            Programar.search(vm.params).then(function (result) {
                archive = result;
                filterProgramar();
            });
        }

        /**
         * Concatena nome das contas do header da tabela
         */
        function makeNameDefaultContas()
        {
            if (vm.selectedContas.length) {
                var nomes = vm.selectedContas.map(function (values) {
                    return values.nome;
                });
                vm.defaultContas = 'Contas: ' + nomes.join(', ');
            }
        }

        function filterProgramar()
        {
            vm.limitTo = 0;
            vm.filteredProgramar = $filter('filter')(archive, {'operacao': vm.config.operacao, 'infos': {'compensado': false}});
            loadMore();
            calcBalancete();
        }

        function calcBalancete()
        {
            var despesas, receitas, data;

            data = {
                "previsto": 0,
                "realizado": 0,
                "diff": 0,
                "hoje": 0,
                "atrasados": 0,
                "percent": 0
            };

            despesas = angular.copy(data);
            receitas = angular.copy(data);

            angular.forEach(archive, function (movi) {

                if (movi.operacao === 'D') {//DESPESAS
                    calcItem(despesas, movi);
                } else if (movi.operacao === 'R') {//RECEITAS
                    calcItem(receitas, movi);
                }

            });

            vm.sidebar.balancete = {
                "despesas": calcFinalyItem(despesas),
                "receitas": calcFinalyItem(receitas)
            };
        }

        function calcItem(data, movi)
        {
            var valor = movi.vl_final;
            data.previsto += valor;

            if (movi.infos.compensado)
                data.realizado += valor;

            if (movi.infos.atrasado)
                data.atrasados += valor;

            if (movi.infos.hoje || movi.infos.atrasado)
                data.hoje += valor;

            return data;
        }

        function calcFinalyItem(data)
        {
            data.diff = data.previsto - data.realizado;
            data.percent = calcPercent(data.realizado, data.previsto);
            return data;
        }

        function calcPercent(from, total)
        {
            var r = 0;

            if (from > 0)
                r = (from * 100) / total;

            return r.toFixed(2);
        }

        /**
         * Para fazer buscas sem filtro de datas
         * Ultimo para funcação searchParcelas/searchRepeats
         * @param object params
         * @param string title
         */
        function simpleQuerySearch(params, title)
        {
            vm.confQuey = {
                'on': true,
                'title': title
            };

            Programar.search(params).then(function (result) {
                archive = result;
                filterProgramar();
            });
        }

        function searchRepeats(movi)
        {
            if (movi.frequencia_id)
                simpleQuerySearch({'frequencia_id': movi.frequencia_id, 'include': 'entidade,conta,planoConta'}, "Movimentações repetidas de  (" + movi.historico + ")");
        }

        function searchParcelas(movi)
        {
            if (movi.pai_id)
                simpleQuerySearch({'pai_id': movi.pai_id, 'include': 'entidade,conta,planoConta'}, "Filtrando por parcelas  de (" + movi.historico + ")");
        }


        function dialogChangeToDate(item, ev)
        {
            var confirm = $mdDialog.confirm()
                    .title('Atualizar data de pagamento?')
                    .textContent('Você gostaria que o sistema atualizasse a data de pagamento deste item para o dia de hoje?')
                    .ariaLabel('Atualizar data de pagamento')
                    .targetEvent(ev)
                    .ok('Sim, atualize')
                    .cancel('Não, deixe como está');

            return $mdDialog.show(confirm).then(
                    function () {
                        return  {'status': true, 'dt_compensado': new Date()};
                    },
                    function () {
                        return {'status': true, 'dt_compensado': item.dt_prevista};
                    }
            );
        }

        function updatePartials(item, data)
        {
            Programar.update(item.id, data).then(function (result) {
                loadProgramar();
                angular.extend(item, result);
            });
        }

        function updateItem(item, ev)
        {

            if (item.status) {

                updatePartials(item, {
                    'status': false,
                    'dt_compensado': null
                });

            } else {

                var d = w3Date.diffDays(item.dt_prevista);

                if (item.dt_prevista && d !== 0) {

                    dialogChangeToDate(item, ev).then(function (data) {
                        updatePartials(item, data);
                    });

                } else {

                    updatePartials(item, {
                        'status': true,
                        'dt_compensado': new Date()
                    });

                }

            }

        }


        /**
         * Open new contact dialog
         *
         * @param ev
         * @param contact
         */
        function openProgramarDialog(ev, row)
        {
            var data = row || vm.config;

            $mdDialog.show({
                controller: 'ProgramarDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'programar/dialogs/programar/programar-dialog.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': data
                }
            }).then(function (result) {
                if (result) {
                    loadProgramar();
                }
            });
        }

        function openBaixarProgDialog(ev, row)
        {
            var data = row || vm.config;

            Programar.dialog.dialogBaixarProg(data, ev).then(function (result) {
                if (result) {
                    loadProgramar();
                }
            });
        }

        function confirmDelete(item, $index, $event)
        {
            w3Utils.confirmDelete('Excluir movimentação', 'Esta ação é irreversível, deseja realmente excluir a movimentação ' + item.referencia + '( R$ ' + item.valor + ' ) ?', $event)
                    .then(function () {
                        Programar.remove(item.id).then(function () {
                            vm.filteredProgramar.splice($index, 1);
                            calcBalancete();
                        });
                    });
        }


        function confirmMultipleDelete(lista, $event)
        {
            var html = "Esta ação é irreversível, deseja realmente excluir as movimentações selecionadas? <br /> <ul>";

            lista.map(function (o) {
                html += "<li>#" + o.id + " - " + o.historico + " " + "(R$ " + o.vl_final + ")</li>";
            });

            html += "</ul>";

            w3Utils.confirmDelete('Excluir movimentações', html, $event)
                    .then(function () {
                        removeAll(lista);
                    });
        }

        function removeAll(lista)
        {
            var tasks = [];

            angular.forEach(lista, function (value) {
                this.push(Programar.remove(value.id));
            }, tasks);

            $q.all(tasks).then(function () {
                loadProgramar();
                vm.selectedProgramar = [];
            });
        }

        /**
         * Toggle selected status of the contact
         *
         * @param programar
         * @param event
         */
        function toggleSelectProgramar(programar, event)
        {
            if (event)
            {
                event.stopPropagation();
            }

            if (vm.selectedProgramar.indexOf(programar) > -1)
            {
                vm.selectedProgramar.splice(vm.selectedProgramar.indexOf(programar), 1);
            } else
            {
                vm.selectedProgramar.push(programar);
            }

            vm.sidebar.totalSelected = vm.selectedProgramar.sum('valor');
        }

        /**
         * Deselect programar
         */
        function deselectProgramar()
        {
            vm.selectedAll = false;
            vm.selectedProgramar = [];
            vm.sidebar.totalSelected = 0;
        }

        /**
         * Sselect all programar
         */
        function selectAllProgramar()
        {
            vm.selectedAll = true;
            vm.selectedProgramar = vm.filteredProgramar;
            vm.sidebar.totalSelected = vm.selectedProgramar.sum('valor');
        }

        function loadFormasPagamento()
        {
            return FormaPagamento.all().then(function (result) {
                return vm.formas_pagamento = result;
            });
        }

        /*
         * QUERY SEARCHS
         */

        // Search contas bancárias
        function querySearchConta(query)
        {
            return Conta.querySearch(query);
        }

        // Search favorecidos/entidades
        function querySearchEntidade(query)
        {
            return Entidade.querySearch(query);
        }

        // Search planos de conta
        function querySearchPlanos(query)
        {
            return PlanoConta.search({q:query});
        }

        // Search centros de custo
        function querySearchCentroCusto(query)
        {
            return CentroCusto.search({q:query});
        }

        /*CHIPS METHODS */
        function transformChip(chip)
        {
            if (angular.isObject(chip)) {
                return chip;
            }
            return {
                name: chip
            };
        }

        /*
         * Responsável por pegar os is dos chips
         */
        function getIds(colection)
        {
            if (!angular.isArray(colection))
                return null;

            return colection.map(function (row) {
                return row.id;
            }).join('|');
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

        function print()
        {
            alert('IMPRIMIR');
        }
    }

})();
