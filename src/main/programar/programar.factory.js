(function () {
    'use strict';

    angular
            .module('main.programar')
            .factory('Programar', ProgramarService);


    /** @ngInject */
    function ProgramarService(w3Resource, w3Date, w3Presenter, FinanPaths, w3Utils, $http, $mdDialog, $document) {

        var path_pack = FinanPaths.BASE_PATH;
        var urlApi = '/rapi/finan/movimentacao/programados';
        var Presenter = w3Presenter.make(responsePresenter, requestPresenter, paramsPresenter);

        var service = {
            transformRequestParcelas: transformRequestParcelas,
            transformResponseParcelas: transformResponseParcelas,
            baixarProgramacao: baixarProgramacao,
            calcSaldo: calcSaldo,
            updateEncargos: updateEncargos,
            dialog: {
                dialogBaixarProg: dialogBaixarProg
            }
        };

        return w3Resource.extend(urlApi, service, Presenter);

        //-------------------------
        function responsePresenter(response)
        {
            response.vl_final = response.operacao === 'D' ? response.vl_despesa : response.vl_receita;
            response.dt_emissao = w3Date.enToDate(response.dt_emissao);
            response.dt_prevista = w3Date.enToDate(response.dt_prevista);
            response.dt_competencia = w3Date.enToDate(response.dt_competencia);
            response.dt_compensado = w3Date.enToDate(response.dt_compensado);
            response.frequencia_fim = w3Date.enToDate(response.frequencia_fim);
            response.options = (angular.isArray(response.options)) ? {} : response.options;
            return response;
        }

        function requestPresenter(request)
        {
            request.dt_emissao = w3Date.toFormatEn(request.dt_emissao);
            request.dt_prevista = w3Date.toFormatEn(request.dt_prevista);
            request.dt_competencia = w3Date.toFormatEn(request.dt_competencia);
            request.dt_compensado = w3Date.toFormatEn(request.dt_compensado);
            request.frequencia_fim = w3Date.toFormatEn(request.frequencia_fim);

            if (!request.options) {
                delete request.options;
            }

            delete request.entidade;
            delete request.infos;
            delete request.conta;

            return request;
        }

        function paramsPresenter(params)
        {
            //Paramentros de filtros
            params.dt_start = w3Date.toFormatEn(params.dt_start);
            params.dt_end = w3Date.toFormatEn(params.dt_end);

            return params;
        }

        function transformRequestParcelas(parcelas)
        {
            if (!parcelas.length) {
                return null;
            } else {
                parcelas.map(function (value) {
                    value.dt_prevista = w3Date.toFormatEn(value.dt_prevista);
                });
                return parcelas;
            }
        }

        function transformResponseParcelas(parcelas)
        {
            if (!parcelas || !parcelas.length) {
                return null;
            } else {
                parcelas.map(function (value) {
                    value.dt_prevista = w3Date.enToDate(value.dt_prevista);
                });
                return parcelas;
            }
        }

        function baixarProgramacao(id, data)
        {
            var dataRequest = Presenter.request(data);

            return $http.put(w3Utils.urlApi(urlApi + '/{id}/baixar', id), dataRequest).then(function (result) {
                return (result.data.status === 'success') ? Presenter.item(result.data.data) : false;
            });
        }

        /**
         * @param int id
         * @param Date dtPagamento
         * @param decimal valor
         * @returns promisse
         */
        function updateEncargos(id, dtPagamento, valor)
        {
            var data = {
                params: {
                    'movimentacao_id': id,
                    'dt_compensado': dtPagamento,
                    'valor': valor
                }
            };

            data.params = Presenter.request(data.params);

            return $http.get(w3Utils.urlApi(urlApi + '/calc-encargos'), data).then(function (result) {
                return Presenter.item(result.data.data);
            });
        }

        function toFloat(valor)
        {
            return parseFloat(valor) || 0;
        }

        function calcSaldo(row)
        {
            return (toFloat(row.valor) + toFloat(row.vl_juros) + toFloat(row.vl_multa)) - toFloat(row.vl_desconto);
        }

        function dialogBaixarProg(data, ev)
        {
            return $mdDialog.show({
                controller: 'BaixarProgDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'programar/dialogs/baixar/baixar-dialog.html',
                parent: angular.element($document.find('#content-container')),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': data
                }
            });
        }


    }

})();
