(function ()
{
    'use strict';

    angular
            .module('main.programar', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
         var path_pack = FinanPaths.BASE_PATH;
         
        $stateProvider.state('xsession.programar', {
            url: '/programar/:operacao',
            checkSessionRequest: true,
            views: {
                'content@app': {
                    templateUrl: path_pack + 'programar/programar.html',
                    controller: 'ProgramarController as vm'
                }
            }
        });

        // Navigation
        msNavigationServiceProvider.saveItem('programar', {
            title: 'Programação',
            icon: 'icon-clock',
            weight: 11
        });

        // Navigation
        msNavigationServiceProvider.saveItem('programar.despesa', {
            title: 'Contas a pagar',
            icon: 'icon-trending-down',
            state: 'xsession.programar({"operacao": "despesa"})',
            weight: 12
        });

        // Navigation
        msNavigationServiceProvider.saveItem('programar.receita', {
            title: 'Contas a receber',
            icon: 'icon-trending-up',
            state: 'xsession.programar({"operacao": "receita"})',
            weight: 13
        });

    }

})();