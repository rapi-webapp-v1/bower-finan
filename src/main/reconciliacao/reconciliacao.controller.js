(function () {
    'use strict';

    angular
            .module('main.reconciliacao')
            .controller('ReconciliacaoController', ReconciliacaoController);

    /** @ngInject */
    function ReconciliacaoController(Reconciliacao) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'created_at', //EX: nome,-created_at
            'q' : null,
            'include' : 'conta,profile'
        };

        // Methods
        vm.search        = search;
        vm.setPage       = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Reconciliacao.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Reconciliacao.pagination;
            });

        }

        function setPage(p)
        {
            Reconciliacao.setPage(p);
            activate();
        }

        function search()
        {
            Reconciliacao.setPage(1);
            activate();
        }

    }


})();
