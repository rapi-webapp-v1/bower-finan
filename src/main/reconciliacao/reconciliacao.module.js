(function ()
{
    'use strict';

    angular
            .module('main.reconciliacao', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        // State
        $stateProvider
                .state('xsession.reconciliacao', {
                    url: '/conciliacoes',  
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack + 'reconciliacao/reconciliacao-archive.html',
                            controller: 'ReconciliacaoController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('reconciliacao', {
            title: 'Conciliação',
            icon: 'icon-check-circle',
            state: 'xsession.reconciliacao',
            weight: 25
        });

    }
})();
