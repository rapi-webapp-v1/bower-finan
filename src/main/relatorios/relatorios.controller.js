(function ()
{
    'use strict';

    angular
            .module('main.relatorios')
            .controller('RelatoriosController', RelatoriosController);

    /** @ngInject */
    function RelatoriosController(FinanPaths, FormaPagamento, w3Utils, $element, xSession, Conta, Entidade, PlanoConta, CentroCusto, Variables, Report, w3Date)
    {
        var vm = this;
        var path_pack = FinanPaths.BASE_PATH;

        // Data
        vm.selectedStatus = [];
        vm.contas = [];
        vm.planos = [];
        vm.titleReport = null;
        vm.sidenavlist = [
            {
                name: 'Conta',
                itens: [
                    {
                        'label': 'Saldo de contas',
                        'action': 'saldo-contas',
                        'multiples': ['conta_id'],
                        'on': ['conta_id', 'dt_end'],
                        'required': []
                    }
                ]
            },
            {
                name: 'Lançamentos',
                itens: [
                    {
                        'label': 'Extrato',
                        'action': 'extrato-lancamentos',
                        'multiples': ['entidade_id', 'plano_conta_id', 'centro_custo_id'],
                        'on': [
                            'conta_id',
                            'entidade_id',
                            'plano_conta_id',
                            'centro_custo_id',
                            'forma_pagamento_id',
                            'frequencia_id',
                            'operacao',
                            'status',
                            'date_report'
                        ],
                        'required': ['conta_id']
                    },
                    {
                        'label': 'DRE',
                        'action': 'dre',
                        'multiples': ['conta_id', 'entidade_id', 'plano_conta_id', 'centro_custo_id'],
                        'on': [
                            'conta_id',
                            'entidade_id',
                            'plano_conta_id',
                            'centro_custo_id',
                            'forma_pagamento_id',
                            'frequencia_id',
                            'operacao',
                            'status',
                            'date_report'
                        ],
                        'required': ['conta_id']
                    }

                ]
            },
            {
                name: 'Programação',
                itens: [
                    {
                        'label': 'Movimentações programadas',
                        'action': 'movimentacao-programadas',
                        'multiples': ['conta_id'],
                        'on': [
                            'conta_id',
                            'operacao',
                            'entidade_id',
                            'plano_conta_id',
                            'centro_custo_id',
                            'forma_pagamento_id',
                            'frequencia_id',
                            'date_report'
                        ],
                        'required': ['conta_id', 'operacao']
                    }
                ]
            }
        ];

        vm.optionsDate = Variables.dates;

        vm.params = {
            'q': null,
            'include': '',
            'ocultar': true
        };
        vm.reportQuery = Report.newQuery();
        vm.path_pack = path_pack;
        vm.selectedContas = [];
        vm.selectedEntidades = [];
        vm.selectedPlanos = [];
        vm.selectedCentrosCustos = [];
        vm.formas_pagamento = [];
        vm.situacoes = [];
        vm.searchTerm = '';
        vm.searchTermPlano = '';

        // Methods
        vm.setReport = setReport;
        vm.loadContas = loadContas;
        vm.loadPlanos = loadPlanos;

        vm.transformChip = transformChip;
        vm.loadFormasPagamento = loadFormasPagamento;
//        vm.querySearchConta = querySearchConta;
        vm.querySearchEntidade = querySearchEntidade;
//        vm.querySearchPlanos = querySearchPlanos;
        vm.querySearchCentroCusto = querySearchCentroCusto;
        vm.transformChip = transformChip;
        vm.setParamsDates = setParamsDates;

        activate();
        //////////


        function activate()
        {
            vm.params.ocultar = true;
            vm.situacoes = Variables.situacao;
            console.log(xSession.ID());
            vm.reportQuery.setParam('livro', xSession.ID());

            $element.find('input').on('keydown', function (ev) {
                ev.stopPropagation();
            });
        }

        function setParamsDates()
        {
            vm.reportQuery.setParam('dt_end', w3Date.toFormatEn(vm.params.dt_end));
        }

        function setReport(item)
        {
            vm.titleReport = item.label;

            vm.reportQuery.setReport(item.action);
            vm.reportQuery.setOnColumns(item.on);
            vm.reportQuery.setRequiredColumns(item.required);
            vm.reportQuery.setOnMultiplesValues(item.multiples);
        }

        /*CHIPS SEARCH METHODS */
        function transformChip(chip)
        {
            if (angular.isObject(chip)) {
                return chip;
            }
            return {
                name: chip
            };
        }

        function loadFormasPagamento()
        {
            return FormaPagamento.all().then(function (result) {
                return vm.formas_pagamento = result;
            });
        }

        function loadContas()
        {
            if (vm.contas.length) {
                return;
            }
            ;

            var conta_params = {
                'paginate': null,
                'sort': 'nome'
            };
            Conta.search(conta_params).then(function (result) {
                vm.contas = result;
            });
        }

        function loadPlanos()
        {
            if (vm.planos.length) {
                return;
            }
            ;

            var plano_params = {
                'paginate': null,
                'sort': 'codigo',
                'filter': 'onlyPai',
                'include': 'filhos.filhos'
            };
            PlanoConta.search(plano_params).then(function (result) {
                vm.planos = result;
                console.log(vm.planos);
            });
        }

        /*
         * QUERY SEARCHS
         */

        // Search contas bancárias
//        function querySearchConta(query)
//        {
//            return Conta.querySearch(query);
//        }

        // Search favorecidos/entidades
        function querySearchEntidade(query)
        {
            return Entidade.querySearch(query);
        }

        // Search planos de conta
        function querySearchPlanos(query)
        {
            return PlanoConta.querySearch(query);
        }

        // Search centros de custo
        function querySearchCentroCusto(query)
        {
            return CentroCusto.querySearch(query);
        }



    }

})();
