(function ()
{
    'use strict';

    angular
        .module('main.relatorios', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;
        $stateProvider.state('xsession.relatorios', {
            url  : '/relatorios',
            checkSessionRequest: true,
            views: {
                'content@app': {
                    templateUrl: path_pack + 'relatorios/relatorios.html',
                    controller : 'RelatoriosController as vm'
                }
            }
        });
         msNavigationServiceProvider.saveItem('relatorios', {
              title: 'Relatórios',
              icon: 'icon-chart-pie',
              state: 'xsession.relatorios',
              weight: 20
            });
    }

})();