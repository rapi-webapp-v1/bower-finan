(function () {
    'use strict';

    angular
            .module('main.tipo_conta')
            .controller('TipoContaDialogController', TipoContaDialogController);

    /** @ngInject */
    function TipoContaDialogController($mdDialog, TipoConta, row, archive) {

        var vm = this;

        //Data
        vm.isNew = true;
        vm.row = angular.copy(row);
        vm.title = null;
        vm.tipos = [
            {key: 'dinheiro', value: 'Dinheiro'},
            {key: 'conta_corrente', value: 'Conta corrente'},
            {key: 'cartao', value: 'Cartão de crédito'},
            {key: 'poupanca', value: 'Poupança'},
            {key: 'cobranca_bancaria', value: 'Cobrança bancaria'}
        ];

        //Methods
        vm.closeDialog = closeDialog;
        vm.update = update;
        vm.save = save;

        activate();
        //-----------------------------------

        function activate() {
            archive = archive || [];

            if (vm.row && vm.row.id) {
                vm.isNew = false;
            }
        }

        function update() {
            vm.row.include = 'presenter';
            TipoConta.update(row.id, vm.row).then(function (result) {
                if (result) {                    
                    angular.extend(row, result);
                    closeDialog(row);
                }
            });
        }

        function save() {
            vm.row.include = 'presenter';
            TipoConta.save(vm.row).then(function (result) {
                if (result) {

                    archive.push(result);
                    closeDialog(result);
                }
            });
        }

        function closeDialog(params) {
            $mdDialog.hide(params);
        }

    }


})();
