(function () {
    'use strict';

    angular
            .module('main.tipo_conta')
            .controller('TipoContaController', TipoContaController);

    /** @ngInject */
    function TipoContaController(TipoConta, w3Utils, $translate) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.params = {
            'paginate': 10,
            'sort': 'nome', //EX: nome,-created_at
            'q' : null,
            'include': 'presenter'
        };

        // Methods
        vm.showCreate    = showCreate;
        vm.showEdit      = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search        = search;
        vm.setPage       = setPage;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            TipoConta.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = TipoConta.pagination;
            });

        }

        function setPage(p)
        {
            TipoConta.setPage(p);
            activate();
        }

        function search()
        {
            TipoConta.setPage(1);
            activate();
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            TipoConta.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            TipoConta.dialog.edit(row, vm.archive, ev);
        }

       function confirmDelete(item, $index, $event) {

            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete($translate.instant('TIPO_CONTA.TITLE_CONFIRM_DELETE'),$translate.instant('TIPO_CONTA.MSG_CONFIRM_DELETE') + item.nome + '?', $event)
                    .then(function () {
                        TipoConta.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

    }


})();
