(function () {
    'use strict';

    angular
            .module('main.tipo_conta')
            .factory('TipoConta', tipo_contaService);


    /** @ngInject */
    function tipo_contaService($mdDialog, w3Resource, FinanPaths) {

        var urlApi = '/rapi/finan/tipo-contas';
        var path_pack = FinanPaths.BASE_PATH;
        var service = {
            dialog: {
                create: showCreateDialog,
                edit: showDialog
            }
        };

        return w3Resource.extend(urlApi, service);

        // ///////////////////////////////

        // DIALOGS

        /**
         * archive: array com a lista de itens
         * data: pode ser undfined, porem se passsar um objeto ja vai abrir o dialog com dados preenchidos, ideal para passar includes tmb
         * */
        function showCreateDialog(archive, ev, data) {
            return showDialog(data, archive, ev);
        }

        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(row, archive, ev) {

            return $mdDialog.show({
                controller: 'TipoContaDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'tipo_conta/dialogs/tipo_conta-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    'row': row,
                    'archive': archive
                }

            });

        }

    }

})();
