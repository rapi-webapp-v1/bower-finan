(function ()
{
    'use strict';

    angular
        .module('main.tipo_conta', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
         var path_pack = FinanPaths.BASE_PATH;
         
        // State
        $stateProvider
            .state('app.tipo_conta', {
                url    : '/tipo_contas',
                views  : {
                    'content@app': {
                        templateUrl: path_pack + 'tipo_conta/tipo_conta-archive.html',
                        controller : 'TipoContaController as vm'
                    }
                }
            });

            msNavigationServiceProvider.saveItem('configuracoes.tipo_conta', {
              title: 'TIPO_CONTA.MENU',
              icon: 'icon-chart-arc',
              state: 'app.tipo_conta',
              weight: 35
            });

    }
})();
