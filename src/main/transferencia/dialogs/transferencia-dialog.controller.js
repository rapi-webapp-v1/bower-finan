(function() {
  'use strict';

  angular
    .module('main.transferencia')
    .controller('TransferenciaDialogController', TransferenciaDialogController);

  /** @ngInject */
  function TransferenciaDialogController($mdDialog, archive, Conta, Transferencia) {

    var vm = this;

    //Data
    vm.isNew = true;
    vm.row = {
      saldo_inicial: 0,
      dt_transferencia: new Date()
    };
    vm.title = null;
    vm.contas = [];

    //Methods
    vm.closeDialog = closeDialog;
    vm.save = save;

    activate();
    //-----------------------------------

    function activate()
    {
      archive = archive || [];
      loadContas();
    }

    function save()
    {
      vm.row.include = 'fromConta,toConta';

      Transferencia.save(vm.row).then(function(result) {
        if (result) {
          archive.push(result);
          closeDialog(result);
        }
      });
    }

    function closeDialog(params)
    {
      $mdDialog.hide(params);
    }

    function loadContas()
    {
      Conta.all().then(function(result) {
        vm.contas = result;
      });
    }


  }


})();
