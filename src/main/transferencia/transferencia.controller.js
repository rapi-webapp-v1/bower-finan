(function () {
    'use strict';

    angular
            .module('main.transferencia')
            .controller('TransferenciaController', TransferenciaController);

    /** @ngInject */
    function TransferenciaController(Transferencia, w3Utils, $translate, Conta) {
        var vm = this;

        // Data
        vm.archive = [];
        vm.loading = false;
        vm.pagination = {};
        vm.selectedContasDestino = [];
        vm.selectedContasOrigem = [];

        var defaultParams = {
            'paginate': 10,
            'sort': '-created_at', //EX: nome,-created_at
            'q': null,
            'include': 'fromConta,toConta',
            'from_conta_id': null,
            'to_conta_id': null,
            'dt_column': 'created_at',
            'dt_start': null,
            'dt_end': null

        };
        vm.params = angular.copy(defaultParams);

        // Methods
        vm.showCreate = showCreate;
        vm.showEdit = showEdit;
        vm.confirmDelete = confirmDelete;
        vm.search = search;
        vm.setPage = setPage;
        vm.querySearchConta = querySearchConta;
        vm.transformChip = transformChip;
        vm.clearFilter = clearFilter;

        activate();

        //-----------------------

        function activate() {
            vm.loading = true;
            vm.archive = null;

            Transferencia.setPage(1);
            search();

        }

        function setPage(p)
        {
            Transferencia.setPage(p);
            search();
        }


        function search()
        {
            if (vm.selectedContasDestino) {
                vm.params.to_conta_id = getIds(vm.selectedContasDestino);
            }
            if (vm.selectedContasOrigem) {
                vm.params.from_conta_id = getIds(vm.selectedContasOrigem);
            }
            Transferencia.search(vm.params).then(function (result) {
                vm.loading = false;
                vm.archive = result;
                vm.pagination = Transferencia.pagination;
            });
        }

        /**
         * Open new item dialog
         *
         * @param ev
         */
        function showCreate(ev) {
            Transferencia.dialog.create(vm.archive, ev);
        }

        function showEdit(row, ev) {
            Transferencia.dialog.edit(row, vm.archive, ev);
        }

        function confirmDelete(item, $index, $event) {
            console.log(item);
            //TRANSLATE: Você realmente quer apagar este item?
            w3Utils.confirmDelete('Apagar transferencia', $translate.instant('TRANSFERENCIA.MSG_CONFIRM_DELETE') + '  ' + item.fromConta.nome + ' -> '+ item.toConta.nome + ' no valor de R$ '+ item.valor, $event)
                    .then(function () {
                        Transferencia.remove(item.id).then(function () {
                            vm.archive.splice($index, 1);
                        });
                    });

        }

        // Search contas bancárias
        function querySearchConta(query)
        {
            return Conta.querySearch(query);
        }



        /*CHIPS METHODS */
        function transformChip(chip)
        {
            if (angular.isObject(chip)) {
                return chip;
            }
            return {
                name: chip
            };
        }

        /*
         * Responsável por pegar os is dos chips
         */
        function getIds(colection)
        {
            if (!angular.isArray(colection))
                return null;

            return colection.map(function (row) {
                return row.id;
            }).join('|');
        }
        function clearFilter()
        {
            vm.searchActive = false;

            vm.params = angular.copy(defaultParams);
            vm.selectedContasDestino = [];
            vm.selectedContasOrigem = [];

            search();
        }


    }


})();
