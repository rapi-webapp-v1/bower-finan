(function () {
    'use strict';

    angular
            .module('main.transferencia')
            .factory('Transferencia', transferenciaService);


    /** @ngInject */
    function transferenciaService($mdDialog, w3Resource, w3Presenter, FinanPaths, w3Date) {

        var urlApi = '/rapi/finan/transferencias';
        var Presenter = w3Presenter.make(responsePresenter, requestPresenter);

        var path_pack = FinanPaths.BASE_PATH;
        var service = {
            querySearch: querySearch,
            dialog: {
                create: showDialog
            }
        };

        return w3Resource.extend(urlApi, service, Presenter);

        // ///////////////////////////////
        function responsePresenter(data)
        {
            data.dt_transferencia = w3Date.enToObj(data.dt_transferencia);
            return data;
        }

        function requestPresenter(data)
        {
            data.dt_transferencia = w3Date.dateToEn(data.dt_transferencia);
            return data;
        }

        function querySearch(query)
        {
            var params = {
                q: query,
                take: 10
            };

            return this.search(params);
        }



        /** Abre um Dialog para cadastr/edição de um item
         * row: objeto item que está sendo editado
         *  archive: array com a lista de itens
         */
        function showDialog(archive, ev) {

            return $mdDialog.show({
                controller: 'TransferenciaDialogController',
                controllerAs: 'vm',
                templateUrl: path_pack + 'transferencia/dialogs/transferencia-dialog.html',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
//                    'row': row,
                    'archive': archive
                }

            });

        }

    }

})();
