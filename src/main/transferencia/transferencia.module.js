(function ()
{
    'use strict';

    angular
            .module('main.transferencia', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, msNavigationServiceProvider, FinanPaths)
    {
        var path_pack = FinanPaths.BASE_PATH;

        // State
        $stateProvider
                .state('xsession.transferencia', {
                    url: '/transferencias',
                    checkSessionRequest: true,
                    views: {
                        'content@app': {
                            templateUrl: path_pack +'transferencia/transferencia-archive.html',
                            controller: 'TransferenciaController as vm'
                        }
                    }
                });

        msNavigationServiceProvider.saveItem('transferencia', {
            title: 'TRANSFERENCIA.MENU',
            icon: 'icon-swap-horizontal',
            state: 'xsession.transferencia',
            weight: 15
        });

    }
})();
